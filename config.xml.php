<?xml version="1.0"?>
<?php die(); // DO NOT REMOVE THIS LINE ?>
<isis>

	<debugMode>false</debugMode>

	<users>
		<!-- Generate new users for the control panel with admin/usergen.php -->
	</users>

	<dataStore>
		<engine>xmlfile</engine>
		<database>
			<engine>mysql</engine>
			<host></host>
			<port></port>
			<name>isis</name>
			<username></username>
			<password></password>
		</database>
	</dataStore>

	<map>
		<coordinateDigits>3</coordinateDigits>
		<enforceBounds>true</enforceBounds>
		<panFactor>0.25</panFactor>
		<scaleFactor>0.2</scaleFactor>

		<latNorth>42.1</latNorth>
		<latSouth>34.9</latSouth>
		<longWest>-120.6</longWest>
		<longEast>-113.4</longEast>

		<defaultFrameLongW>-117.75</defaultFrameLongW>
		<defaultFrameLatN>38.35</defaultFrameLatN>
		<defaultFrameW>3</defaultFrameW>
		<defaultFrameH>2</defaultFrameH>
		<resizerThickness>0.22</resizerThickness>

		<initialView>
			<latNorth>39.7</latNorth>
			<latSouth>35</latSouth>
			<longWest>-118.6</longWest>
			<longEast>-113.9</longEast>
		</initialView>

		<minCoverage>
			<ns>0.25</ns>
			<ew>0.25</ew>
		</minCoverage>
	</map>

	<results>
		<gridMaxWords>50</gridMaxWords>
		<tileMaxWords>16</tileMaxWords>
		<gridRows>20</gridRows>
		<tileRows>5</tileRows>
		<tileCols>3</tileCols>
		<mapOutlinesLimit>1000</mapOutlinesLimit>

		<displayFields>
			<field mapping="index">
				<display>true</display>
				<name/>
			</field>
			<field mapping="thumb">
				<display>true</display>
				<name/>
			</field>
			<field mapping="pointer">
				<display>false</display>
				<name>Pointer</name>
			</field>
			<field mapping="alias">
				<display>false</display>
				<name>Alias</name>
			</field>
			<field mapping="dc:title">
				<display>true</display>
				<name>Title</name>
			</field>
			<field mapping="dc:creator">
				<display>false</display>
				<name>Creator</name>
			</field>
			<field mapping="dc:date">
				<display>true</display>
				<name>Date</name>
			</field>
			<field mapping="dc:publisher">
				<display>false</display>
				<name>Publisher</name>
			</field>
			<field mapping="dc:source">
				<display>false</display>
				<name>Source</name>
			</field>
			<field mapping="dc:description">
				<display>false</display>
				<name>Description</name>
			</field>
			<field mapping="dc:subject">
				<display>false</display>
				<name>Subject</name>
			</field>
			<field mapping="dc:coverage">
				<display>false</display>
				<name>Coverage</name>
			</field>
			<field mapping="dc:format">
				<display>false</display>
				<name>Format</name>
			</field>
			<field mapping="dc:contributor">
				<display>false</display>
				<name>Contributor</name>
			</field>
			<field mapping="dc:type">
				<display>false</display>
				<name>Type</name>
			</field>
			<field mapping="dc:rights">
				<display>false</display>
				<name>Rights</name>
			</field>
			<field mapping="dc:identifier">
				<display>false</display>
				<name>Identifier</name>
			</field>
		</displayFields>
	</results>
</isis>
