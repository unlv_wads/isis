<?php
/*****************************************************************************
Copyright © 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

interface iResultsView {
	function getFormattedResults(SpatialSearch $s);
	function getResultsPerPage();
}

interface iResultsHTMLView {
	function getPageLinks(SpatialSearch $s);
	function getSortLinks(SpatialSearch $s);
	function getViewLinks();
}

abstract class ResultsView extends View {

	public function getGroupLinks() {
		$new_get = $_GET;
		unset($new_get['group']);

		switch ($_GET['group']) {
			case 'true':
				unset($new_get['group']);
				$new_qs = http_build_query($new_get);
				$group_links = sprintf('<a href="%s">Grouped By Collection</a>',
					$_SERVER['PHP_SELF'] . '?' . $new_qs);
				break;
			default:
				$new_get['group'] = 'true';
				$new_qs = http_build_query($new_get);
				$group_links = sprintf('<a href="%s">Ungrouped</a>',
					$_SERVER['PHP_SELF'] . '?' . $new_qs);
				break;
		}
		return $group_links;
	}


	public function getPageLinks(SpatialSearch $s) {
		$rpp = ($this->getResultsPerPage() > 0) ? $this->getResultsPerPage() : 5;
		$numpages = ceil($s->getNumResults() / $rpp);

		$pages = array();
		$current_page = array_key_exists('page', $_GET) ? (int) $_GET['page'] : 1;
		$new_get = $_GET;
		for ($page = 1; $page <= $numpages; $page++) {
			$new_get['page'] = $page;
			if ($page == $current_page) {
				$pages[] = '<strong>' . $page . '</strong>';
			}
			else {
				$pages[] = sprintf('<a href="%s">%s</a>',
					'?' . http_build_query($new_get),
					$page
				);
			}
		}
		return implode(' ', $pages);
	}


	public function getSortLinks(SpatialSearch $s) {
		$new_get = $_GET;
		$items = array();
		foreach (SpatialSearch::$valid_sorts as $url => $name) {
			if ($s->getSort() == $url) {
				if ($s->getSortOrder() == 'desc') {
					$new_get['sort'] = $url;
					unset($new_get['order']);
					$items[] = sprintf('<a href="?%s">&darr; %s</a>',
						http_build_query($new_get), $name);
				}
				else {
					$new_get['sort'] = $url;
					$new_get['order'] = 'desc';
					$items[] = sprintf('<a href="?%s">&uarr; %s</em>',
						http_build_query($new_get), $name);
				}
			}
			else {
				$new_get['sort'] = $url;
				unset($new_get['order']);
				$items[] = sprintf('<a href="?%s">%s</a>',
					http_build_query($new_get), $name
				);
			}
		}
		return implode(' | ', $items);
	}


	public function getViewLinks() {
		$new_get = $_GET;
		unset($new_get['page']);

		switch ($_GET['view']) {
			case 'tile':
				unset($new_get['view']);
				$new_qs = http_build_query($new_get);
				$view_links = sprintf('<a href="%s">Grid</a> | <em>Tile</em>',
					$_SERVER['PHP_SELF'] . '?' . $new_qs);
				break;
			default:
				$new_get['view'] = 'tile';
				$new_qs = http_build_query($new_get);
				$view_links = sprintf('<em>Grid</em> | <a href="%s">Tile</a>',
					$_SERVER['PHP_SELF'] . '?' . $new_qs);
				break;
		}
		return $view_links;
	}

} // ResultsView

?>
