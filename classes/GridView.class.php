<?php
/*****************************************************************************
Copyright © 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

class GridView extends ResultsView implements iResultsView, iResultsHTMLView {

	public function getFormattedResults(SpatialSearch $s) {
		$results = $s->getResults();
		$fields = Preference::getInstance()->getDisplayFields();

		// assemble header
		$header = array();
		foreach ($fields as $f) {
			$header[] = '<th>' . String::websafe($f->getName()) . '</th>';
		}
		$header = '<tr>' . implode('', $header) . '</tr>';

		// assemble table body
		$max = min(array($this->getResultsPerPage(), sizeof($results)));
		$body = array();
		for ($i = 0; $i < $max; $i++) {

			// collection grouping
			$cid = $results[$i]->getCollectionID();
			if ($s->isGroupingByCollection() && $cid <> $last_cid) {
				$body[] = sprintf('<tr><td class="colGroupDivider" colspan="%d">
						<strong><a href="%s">%s</a></strong>
						<br/>(<a href="%s">%s</a>)</td></tr>',
					count($fields),
					String::websafe($results[$i]->getCollection()->getIntroURL()),
					String::websafe($results[$i]->getCollection()->getName()),
					String::websafe($results[$i]->getCollection()->getOrganizationURL()),
					String::websafe($results[$i]->getCollection()->getOrganization())
				);
			}

			$cols = array();
			foreach ($fields as $f) {
				try {
					if ($f->getMapping() == 'index') {
						$cols[] = sprintf('<td>%d</td>',
							$s->getResultIndex() + $i
						);
					}
					else if ($f->getMapping() == 'thumb') {
						$cols[] = sprintf('<td><a href="%s"><img src="%s"
								alt="map thumbnail" class="resultsThumb"
								onmouseover="HighlightMap(\'%s\');
									return false;" /></a></td>',
							String::websafe($results[$i]->getRecordURL()),
							String::websafe($results[$i]->getThumbURL()),
							$results[$i]->getCollection()->getID() . '-'
								. $results[$i]->getPtr()
						);
					}
					else if ($f->getMapping() == 'pointer') {
						$cols[] = sprintf('<td>%s</td>',
							String::websafe($results[$i]->getPtr())
						);
					}
					else if ($f->getMapping() == 'alias') {
						$cols[] = sprintf('<td>%s</td>',
							String::websafe($results[$i]->getCollection()->getAlias())
						);
					}
					else {
						$cols[] = sprintf('<td><a href="%s">%s</a></td>',
							String::websafe($results[$i]->getRecordURL()),
							String::websafe(
								String::truncate($results[$i]->getMetadata($f->getMapping()),
								Preference::getInstance()->getGridMaxWords()))
						);
					}
				}
				catch (UnavailableMapException $e) {
					$cols[] = '<td>Unavailable map</td>';
				}
			}
			$body[] = '<tr>' . implode('', $cols) . '</tr>';

			$last_cid = $cid;
		}
		$body = implode('', $body);

		// return assembled table
		return '<table cellpadding="0" cellspacing="0" border="0"
			id="resultsListTable">' . $header . $body . '</table>';
	}


	public function getResultsPerPage() {
		return Preference::getInstance()->getGridRows();
	}

} // GridView

?>
