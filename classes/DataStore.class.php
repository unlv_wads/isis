<?php
/*****************************************************************************
Copyright © 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

interface iDataStore {
	function getAllCollections();
	function getAllCollectionsOutside($lat_n, $lat_s, $long_e, $long_w);
	function getAllCollectionsPartiallyWithin($lat_n, $lat_s, $long_e, $long_w);
	function getAllCollectionsWithin($lat_n, $lat_s, $long_e, $long_w);
	function getAllMaps($page, $results_per_page, $sort, $sort_order);
	function getAllMapsAsArray();
	function getAllMapsOutside($lat_n, $lat_s, $long_e, $long_w, $page,
		$results_per_page, $sort, $sort_order, $group_by_collection,
		array $collection_ids);
	function getAllMapsPartiallyWithin($lat_n, $lat_s, $long_e, $long_w, $page,
		$results_per_page, $sort, $sort_order, $group_by_collection,
		array $collection_ids);
	function getAllMapsWithin($lat_n, $lat_s, $long_e, $long_w, $page,
		$results_per_page, $sort, $sort_order, $group_by_collection,
		array $collection_ids);
	function deleteCollection(Collection $c);
	function saveCollection(Collection $c);
	function loadCollectionProperties(Collection $c);
	function collectionHasSiblingWithSameNameAndURL(Collection $c);
	function collectionHasSiblingWithSameID(Collection $c);
	function mapHasSiblingWithSameCollectionAndPtr(Map $m);
	function mapHasSiblingWithSameID(Map $m);
	function deleteMap(Map $m);
	function saveMap(Map $m);
	function loadMapProperties(Map $m);
	function getName();
	function getNumMaps();
	function getNumMapsOutside($lat_n, $lat_s, $long_e, $long_w,
		array $collection_ids);
	function getNumMapsPartiallyWithin($lat_n, $lat_s, $long_e, $long_w,
		array $collection_ids);
	function getNumMapsWithin($lat_n, $lat_s, $long_e, $long_w,
		array $collection_ids);
}

class DataStore {

	private static $available_engines = array('rdbms', 'xmlfile');
	private static $instance;	// self object


	protected function __construct() {}
	public function __clone() {}
	public function __wakeup() {}


	public static final function isAvailable() {
		if (!in_array(Preference::getInstance()->getStorageEngine(),
		self::getAvailableEngines())) {
			return false;
		}
		if (self::getInstance() instanceof XMLDataStore) {
			try {
				$sxml = self::getInstance()->load();
			}
			catch (XMLDataStoreException $e) {
				return false; // do nothing, as we are just checking
			}
		}
		else if (self::getInstance() instanceof RDBMSDataStore) {
			try {
				$dbcon = self::getInstance()->getConnection();
			}
			catch (DatabaseErrorException $e) {
				return false; // do nothing, as we are just checking
			}
		}
		return true;
	}


	public static final function getAvailableEngines() {
		return self::$available_engines;
	}


	public static function getInstance() {
		if (!is_object(self::$instance)) {
			switch (Preference::getInstance()->getStorageEngine()) {
				case 'xmlfile':
					self::$instance = XMLDataStore::getInstance(); break;
				case 'rdbms':
					self::$instance = RDBMSDataStore::getInstance(); break;
			}
		}
		return self::$instance;
	}

}

?>
