<?php
/*****************************************************************************
Copyright © 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

interface iUser {
	function getID();
	function getUsername();
	function login($password);
}

class User implements iUser {

	private $id, $username;


	public function __construct($username) {
		$this->setUsername($username);
	}


	public final function getID() {
		return $this->id;
	}


	private function setID($int) {
		$this->id = $int;
	}


	public final function getInstance() {
		if ($_SESSION['user'] instanceof User) {
			return $_SESSION['user'];
		}
		return false;
	}


	public static final function isLoggedIn() {
		return ($_SESSION['user'] instanceof User) ? true : false;
	}


	public final function login($password) {
		if (Preference::getInstance()->loginIsValid(
		$this->getUsername(), $password)) {
			$_SESSION['user'] = $this;
			return true;
		}
		return false;
	} // login()


	public final function logout() {
		unset($_SESSION['user'], $this);
	}


	public final function getUsername() {
		return $this->username;
	}


	private function setUsername($str) {
		$this->username = $str;
	}

} // User

?>
