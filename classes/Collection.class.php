<?php
/*****************************************************************************
Copyright © 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

interface iCollection {
	function __construct();
	function save();
	function getAlias();
	function setAlias($str);
	function getBaseURL();
	function setBaseURL($str);
	function getID();
	function setID($int);
	function existsInCONTENTdm();
	function getIntroURL();
	function setIntroURL($str);
	function getName();
	function setName($str);
	function getOAIIDPrefix();
	function setOAIIDPrefix($str);
	function getOrganization();
	function setOrganization($str);
	function getOrganizationURL();
	function setOrganizationURL($str);
	function getPathToOAI();
	function setPathToOAI($str);
	function getPathToThumbnail();
	function setPathToThumbnail($str);
	function hasSiblingWithSameNameAndURLInDataStore();
	function hasSiblingWithSameIDInDataStore();
}

class Collection implements iCollection {

	private $id, $alias, $base_url, $intro_url, $name, $oai_id_prefix,
		$organization, $organization_url, $path_to_oai, $path_to_thumbnail;


	public function __construct($id=null) {
		if ($id) {
			$this->setID($id);
			$this->instantiateSelf();
		}
	}


	public final function save() {
		return DataStore::getInstance()->saveCollection($this);
	}


	public final function getAlias() {
		return $this->alias;
	}


	public final function setAlias($str) {
		$this->alias = '/' . trim($str, '/');
	}


	public static final function getAll() {
		return DataStore::getInstance()->getAllCollections();
	}


	public static function getAllOutside($lat_n, $lat_s, $long_e, $long_w) {
		return DataStore::getInstance()->getAllCollectionsOutside($lat_n, $lat_s,
			$long_e, $long_w);
	}


	public static function getAllPartiallyWithin($lat_n, $lat_s, $long_e,
		$long_w) {
		return DataStore::getInstance()->getAllCollectionsPartiallyWithin($lat_n,
			$lat_s, $long_e, $long_w);
	}


	public static function getAllWithin($lat_n, $lat_s, $long_e, $long_w) {
		return DataStore::getInstance()->getAllCollectionsWithin($lat_n,
			$lat_s, $long_e, $long_w);
	}


	public final function getBaseURL() {
		return $this->base_url;
	}


	public final function setBaseURL($str) {
		$this->base_url = $str;
	}


	public final function getID() {
		return $this->id;
	}


	public final function setID($int) { // access via constructor please
		$this->id = $int;
	}


	public final function existsInCONTENTdm() {
		$url = trim($this->getBaseURL(), '/') . '/'
			. trim($this->getPathToOAI(), '/') . '?verb=ListSets';
		if (!$str = @file_get_contents($url)) {
			throw new UnknownHostException(sprintf(String::NO_OAI_RESPONSE, $url));
			return false;
		}
		$sxml = simplexml_load_string($str);

		$aliases = array();
		foreach ($sxml->ListSets->children() as $s) {
			$aliases[] = '/' . trim((string) $s->setSpec, '/');
		}

		return (in_array($this->getAlias(), $aliases)) ? true : false;
	}


	public final function getIntroURL() {
		return $this->intro_url;
	}


	public final function setIntroURL($str) {
		$this->intro_url = $str;
	}


	public final function getName() {
		return $this->name;
	}


	public final function setName($str) {
		$this->name = $str;
	}


	public final function getOAIIDPrefix() {
		return $this->oai_id_prefix;
	}


	public final function setOAIIDPrefix($str) {
		$this->oai_id_prefix = $str;
	}


	public final function getOrganization() {
		return $this->organization;
	}


	public final function setOrganization($str) {
		$this->organization = $str;
	}


	public final function getOrganizationURL() {
		return $this->organization_url;
	}


	public final function setOrganizationURL($str) {
		$this->organization_url = $str;
	}


	public final function getPathToOAI() {
		return $this->path_to_oai;
	}


	public final function setPathToOAI($str) {
		$this->path_to_oai = '/' . trim($str, '/');
	}


	public final function getPathToThumbnail() {
		return $this->path_to_thumbnail;
	}


	public final function setPathToThumbnail($str) {
		$this->path_to_thumbnail = '/' . trim($str, '/');
	}


	public final function hasSiblingWithSameIDInDataStore() {
		return DataStore::getInstance()->collectionHasSiblingWithSameID($this);
	}


	public final function hasSiblingWithSameNameAndURLInDataStore() {
		return DataStore::getInstance()->collectionHasSiblingWithSameNameAndURL($this);
	}


	private function instantiateSelf() {
		DataStore::getInstance()->loadCollectionProperties($this);
	}

} // Collection

?>
