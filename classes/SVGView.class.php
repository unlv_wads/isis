<?php
/*****************************************************************************
Copyright © 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

class SVGView extends ResultsView implements iResultsView {

	private $canvas;	// SVGCanvas object
	private $frame;	// SVGFrame object (not used in all views)
	private $resizer;	// SVGResizer object (not used in all views)


	public function __construct($x, $y, $width, $height) {
		$this->setCanvas(new SVGCanvas($x, $y, $width, $height));
	}


	public function getCanvas() {
		return $this->canvas;
	}


	public function setCanvas(SVGCanvas $c) {
		$this->canvas = $c;
	}


	public function getFormattedResults(SpatialSearch $s) {
		$results = $s->getResults();

		if (!empty($_GET['lw']) && !empty($_GET['le'])
		&& !empty($_GET['ls']) && !empty($_GET['ln'])) {
			$fx = $_GET['lw'];
			$fy = -$_GET['ln'];
			$fw = $_GET['le'] - $_GET['lw'];
			$fh = $_GET['ln'] - $_GET['ls'];
		}
		else {
			$fx = Preference::getInstance()->getDefaultFrameX();
			$fy = Preference::getInstance()->getDefaultFrameY();
			$fw = Preference::getInstance()->getDefaultFrameWidth();
			$fh = Preference::getInstance()->getDefaultFrameHeight();
		}

		$svg = '<g id="outlines" currentlyHighlighted="null" ';
		if ($s->getNumResults() > 0 && $s->getNumResults() >
			Preference::getInstance()->getResultsMapOutlinesLimit()) {
			$svg .= 'style="stroke:none" ';
		}
		$svg .= '>';

		foreach ($results as $map) {
			$id = sprintf('%d-%d', $map->getCollectionID(), $map->getPtr());
			$svg .= $map->getSVGRect()->getTag($id);
		}

		$svg .= '</g>';

		// Draw the selection
		$frame = new SVGFrame($fx, $fy, $fw, $fh);
		$svg .= sprintf('<rect id="frame" style="cursor:default"
			x="%f" y="%f" width="%f" height="%f" />',
			$frame->getX(), $frame->getY(),
			$frame->getWidth(), $frame->getHeight()
		);

		return $svg;
	}


	public function getFrame() {
		return $this->frame;
	}


	public function setFrame(SVGFrame $f) {
		$this->frame = $f;
	}


	public function getResizer() {
		return $this->resizer;
	}


	public function setResizer(SVGResizer $r) {
		$this->resizer = $r;
	}


	public function getResultsPerPage() {
		return 5000;
	}

} // SVGView

?>
