<?php
/*****************************************************************************
Copyright � 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

abstract class Query {

	const ADD_COLLECTION_SQL = "INSERT INTO collection(alias, base_url,
			intro_url, name, oai_id_prefix, organization, org_url, path_to_oai,
			path_to_thumbnail)
		VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')";
	const ADD_MAP_SQL = "INSERT INTO map(collection_id, ptr, lat_n, lat_s,
		long_e, long_w) VALUES (%d, %d, %f, %f, %f, %f)";
	const ALL_COLLECTIONS_SQL = "SELECT * FROM collection ORDER BY name ASC";
	const ALL_COLLECTIONS_XPATH = "/isis/collections/collection";

	const ALL_COLLECTIONS_OUTSIDE_SQL = "SELECT collection.id
		FROM collection
		LEFT JOIN map ON collection.id = map.collection_id
		WHERE (map.lat_s > %f OR map.lat_n < %f)
			OR (map.long_w > %f OR map.long_e < %f)";

	const ALL_COLLECTIONS_PARTIALLY_WITHIN_SQL = "SELECT collection.id
		FROM collection
		LEFT JOIN map ON collection.id = map.collection_id
		WHERE ((map.long_e >= %f AND map.long_e <= %f)
				OR (map.long_w <= %f AND map.long_w >= %f))
			AND ((map.lat_n >= %f AND map.lat_n <= %f)
				OR (map.lat_s <= %f AND map.lat_s >= %f))";

	const ALL_COLLECTIONS_WITHIN_SQL = "SELECT collection.id
		FROM collection
		LEFT JOIN map ON collection.id = map.collection_id
		WHERE map.lat_s >= %f
			AND map.long_w >= %f
			AND map.lat_n <= %f
			AND map.long_e <= %f";

	const ALL_MAPS_SQL = "SELECT * FROM map
		ORDER BY map.%s %s LIMIT %d OFFSET %d";
	const ALL_MAPS_XPATH = "maps/map";

	const ALL_MAPS_COUNT_SQL = "SELECT COUNT(id) AS count FROM map";

	const ALL_MAPS_OUTSIDE_SQL = "SELECT *,
			(long_e - long_w) * (lat_n - lat_s) AS area,
			lat_s + ((lat_n - lat_s) * 0.5) AS clat,
			long_w + ((long_e - long_w) * 0.5) AS clong
		FROM map
		WHERE (map.lat_s > %f OR map.lat_n < %f)
			OR (map.long_w > %f OR map.long_e < %f)
			%s
		ORDER BY %s %s
		LIMIT %d OFFSET %d";
	const ALL_MAPS_OUTSIDE_XPATH =
		"maps/map[(lat_s > %f or lat_n < %f) or (long_w > %f or long_e < %f)]";

	const ALL_MAPS_OUTSIDE_GROUPED_SQL = "SELECT *,
			(long_e - long_w) * (lat_n - lat_s) AS area,
			lat_s + ((lat_n - lat_s) * 0.5) AS clat,
			long_w + ((long_e - long_w) * 0.5) AS clong
		FROM map
		LEFT JOIN collection ON map.collection_id = collection.id
		WHERE (map.lat_s > %f OR map.lat_n < %f)
			OR (map.long_w > %f OR map.long_e < %f)
			%s
		ORDER BY collection.name ASC, %s %s
		LIMIT %d OFFSET %d";

	const ALL_MAPS_PARTIALLY_WITHIN_SQL = "SELECT *,
			(long_e - long_w) * (lat_n - lat_s) AS area,
			lat_s + ((lat_n - lat_s) * 0.5) AS clat,
			long_w + ((long_e - long_w) * 0.5) AS clong
		FROM map
		WHERE ((map.long_e >= %f AND map.long_e <= %f)
				OR (map.long_w <= %f AND map.long_w >= %f))
			AND ((map.lat_n >= %f AND map.lat_n <= %f)
				OR (map.lat_s <= %f AND map.lat_s >= %f))
			%s
		ORDER BY %s %s
		LIMIT %d OFFSET %d";
	const ALL_MAPS_PARTIALLY_WITHIN_XPATH =
		"maps/map[((long_e >= %f and long_e <= %f) or
			(long_w <= %f and long_w >= %f)) and
			((lat_n >= %f and lat_n <= %f) or
			(lat_s <= %f and lat_s >= %f))]";

	const ALL_MAPS_PARTIALLY_WITHIN_GROUPED_SQL = "SELECT *,
			(long_e - long_w) * (lat_n - lat_s) AS area,
			lat_s + ((lat_n - lat_s) * 0.5) AS clat,
			long_w + ((long_e - long_w) * 0.5) AS clong
		FROM map
		LEFT JOIN collection ON map.collection_id = collection.id
		WHERE ((map.long_e >= %f AND map.long_e <= %f)
				OR (map.long_w <= %f AND map.long_w >= %f))
			AND ((map.lat_n >= %f AND map.lat_n <= %f)
				OR (map.lat_s <= %f AND map.lat_s >= %f))
			%s
		ORDER BY collection.name ASC, %s %s
		LIMIT %d OFFSET %d";

	const ALL_MAPS_WITHIN_SQL = "SELECT *,
			(long_e - long_w) * (lat_n - lat_s) AS area,
			lat_s + ((lat_n - lat_s) * 0.5) AS clat,
			long_w + ((long_e - long_w) * 0.5) AS clong
		FROM map
		WHERE map.lat_s >= %f
			AND map.long_w >= %f
			AND map.lat_n <= %f
			AND map.long_e <= %f
			%s
		ORDER BY %s %s
		LIMIT %d OFFSET %d";
	const ALL_MAPS_WITHIN_XPATH = "maps/map[lat_s >= %f and long_w >= %f
		and lat_n <= %f and long_e <= %f]";

	const ALL_MAPS_WITHIN_GROUPED_SQL = "SELECT *,
			(long_e - long_w) * (lat_n - lat_s) AS area,
			lat_s + ((lat_n - lat_s) * 0.5) AS clat,
			long_w + ((long_e - long_w) * 0.5) AS clong
		FROM map
		LEFT JOIN collection ON map.collection_id = collection.id
		WHERE map.lat_s >= %f
			AND map.long_w >= %f
			AND map.lat_n <= %f
			AND map.long_e <= %f
			%s
		ORDER BY collection.name ASC, %s %s
		LIMIT %d OFFSET %d";

	const COLLECTION_ID_EXISTS_SQL = "SELECT COUNT(id) AS count
		FROM collection WHERE id = %d";
	// for xpath, use GET_COLLECTION_PROPERTIES_XPATH

	const COLLECTION_NAME_URL_EXISTS_SQL = "SELECT COUNT(id) AS count
		FROM collection WHERE name = '%s' OR base_url = '%s'";
	const COLLECTION_NAME_URL_EXISTS_XPATH = "collections/collection[name
		= '%s' or base_url = '%s']";

	const DELETE_COLLECTION_SQL = "DELETE FROM collection WHERE id = %d";
	const DELETE_MAP_SQL = "DELETE FROM map WHERE id = %d";
	const GET_COLLECTION_IDS_XPATH = "collections/collection/id";
	const GET_COLLECTION_PROPERTIES_SQL = "SELECT * FROM collection
		WHERE id = %d";
	const GET_COLLECTION_PROPERTIES_XPATH = "collections/collection[id = %d]";
	const GET_MAP_IDS_XPATH = "maps/map/id";

	const GET_MAP_PROPERTIES_SQL = "SELECT * FROM map WHERE id = %d";
	const GET_MAP_PROPERTIES_XPATH = "maps/map[id = %d]";

	const MAP_COL_PTR_EXISTS_SQL = "SELECT COUNT(id) AS count FROM map
		WHERE ptr = %d AND collection_id = %d";
	const MAP_COL_PTR_EXISTS_XPATH = "maps/map[ptr = %d and collection_id = %d]";

	const MAP_ID_EXISTS_SQL = "SELECT COUNT(id) AS count FROM map WHERE id = %d";
	const MAP_ID_EXISTS_XPATH = "maps/map[id = %d]";

	const NUM_MAPS_OUTSIDE_SQL = "SELECT COUNT(id) AS count FROM map
		WHERE (map.lat_s > %f OR map.lat_n < %f)
			OR (map.long_w > %f OR map.long_e < %f)
			%s";
	// for xpath, count ALL_MAPS_OUTSIDE_XPATH

	const NUM_MAPS_PARTIALLY_WITHIN_SQL = "SELECT COUNT(id) AS count FROM map
		WHERE ((map.long_e >= %f AND map.long_e <= %f)
			OR (map.long_w <= %f AND map.long_w >= %f))
		AND ((map.lat_n >= %f AND map.lat_n <= %f)
			OR (map.lat_s <= %f AND map.lat_s >= %f))
			%s";
	// for xpath, count ALL_MAPS_PARTIALLY_WITHIN_XPATH

	const NUM_MAPS_WITHIN_SQL = "SELECT COUNT(id) AS count FROM map
		WHERE map.lat_s >= %f
			AND map.long_w >= %f
			AND map.lat_n <= %f
			AND map.long_e <= %f
			%s";
	// for xpath, count ALL_MAPS_WITHIN_XPATH

	// no sql for this as users are stored in xml
	const USER_LOGIN_XPATH =
		"//users/user[username = '%s' and password_hash = '%s']";
	const UPDATE_COLLECTION_SQL = "UPDATE collection SET alias = '%s',
		base_url = '%s', intro_url = '%s', name = '%s', oai_id_prefix = '%s',
		organization = '%s', org_url = '%s', path_to_oai = '%s',
		path_to_thumbnail = '%s' WHERE id = %d";
	const UPDATE_MAP_SQL = "UPDATE map SET collection_id = %d, ptr = %d,
		lat_n = %f, lat_s = %f, long_e = %f, long_w = %f WHERE id = %d";

}

?>
