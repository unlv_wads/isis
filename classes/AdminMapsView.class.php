<?php
/*****************************************************************************
Copyright © 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

class AdminMapsView extends AdminView
	implements iResultsView, iResultsHTMLView {

	public function getFormattedResults(SpatialSearch $s) {
		$results = $s->getResults();

		// assemble header
		$header = '<tr><th></th><th></th><th></th><th>Pointer</th>
			<th>Collection</th><th>Title</th><th>Coordinates</th><th>OAI</th></tr>';

		// assemble table body
		$max = min(array($this->getResultsPerPage(), sizeof($results)));
		$body = array();
		for ($i = 0; $i < $max; $i++) { $res = $i;
			$id = String::websafe($results[$i]->getID());
			$cols = array();
			$cols[] = sprintf('<td><input type="checkbox" name="ids[]"
				value="%d" /></td>', $id);
			$cols[] = sprintf('<td><a href="edit.php?id=%d">Edit</a></td>',
				String::websafe($id)
			);
			$cols[] = sprintf('<td style="text-align:center"><img src="%s"
				style="height:60px; max-width:60px" /></td>',
				String::websafe($results[$i]->getThumbURL())
			);
			$cols[] = sprintf('<td>%d</td>',
				String::websafe($results[$i]->getPtr())
			);
			$cols[] = sprintf('<td><a href="%s">%s</a></td>',
				String::websafe($results[$i]->getCollection()->getIntroURL()),
				String::websafe($results[$i]->getCollection()->getName())
			);
			$cols[] = sprintf('<td><a href="%s">%s</a></td>',
				String::websafe($results[$i]->getRecordURL()),
				String::websafe($results[$i]->getMetadata('dc:title'))
			);
			$cols[] = sprintf('<td>%s&deg;N - %s&deg;S / %s&deg;E - %s&deg;W</td>',
				String::websafe($results[$i]->getLatN()),
				String::websafe($results[$i]->getLatS()),
				String::websafe($results[$i]->getLongE()),
				String::websafe($results[$i]->getLongW())
			);
			$cols[] = sprintf('<td><a href="%s">XML</a></td>',
				String::websafe($results[$i]->getOAIRecordURL())
			);
			$body[] = '<tr>' . implode('', $cols) . '</tr>';
		}
		$body = implode('', $body);

		// return assembled table
		return '<table cellpadding="0" cellspacing="0" border="0"
			class="dataTable">' . $header . $body . '</table>';
	}

} // AdminMapsView

?>
