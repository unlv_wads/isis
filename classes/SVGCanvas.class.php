<?php
/*****************************************************************************
Copyright © 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

interface iSVGCanvas {
	function __construct($x, $y, $width, $height);
	function getGraticule();
	function getHeight();
	function setHeight($float);
	function getViewBox();
	function setViewBox($x, $y, $width, $height);
	function getWidth();
	function setWidth($float);
	function getX();
	function setX($float);
	function getY();
	function setY($float);
}

// Class for handling <svg> elements
class SVGCanvas implements iSVGCanvas {

	private $x, $y, $width, $height, $viewbox;


	function __construct($x, $y, $width, $height) {
		$this->setX($x);
		$this->setY($y);
		$this->setWidth($width);
		$this->setHeight($height);
		$this->setViewBox($this->getX(), $this->getY(),
			$this->getWidth(), $this->getHeight());
	}


	public function getGraticule() {
	/* Draws lat/long lines in <path> elements across the extent of the
		canvas. Firefox 2.0 doesn't want to draw these for some reason. */

		for ($lat = -90; $lat <= 90; $lat++) {
			$graticule .= 'M-180' . $lat . 'H180 ';
		}

		for ($long = -180; $long <= 180; $long++) {
			$graticule .= 'M' . $long . '-90 V90 ';
		}

		return sprintf('<path d="%sZ" />', $graticule);
	}

	public function getHeight() {
		return $this->height;
	}

	public function setHeight($float) {
		$this->height = (float) ($float >= 0 && $float <= 90) ? $float : null;
	}

	public function getViewBox() {
		return (strlen($this->viewbox) > 0) ? $this->viewbox :
			self::assembleViewBox($this->getX(), $this->getY(),
				$this->getWidth(), $this->getHeight());
	}

	public function setViewBox($x, $y, $width, $height) {
		$this->viewbox = self::assembleViewBox($x, $y, $width, $height);
	}

	private static function assembleViewBox($x, $y, $width, $height) {
		return implode(' ', array($x, $y, $width, $height));
	}

	public function getWidth() {
		return $this->width;
	}

	public function setWidth($float) {
		$this->width = (float) ($float >= 0 && $float <= 180) ? $float : null;
	}

	public function getX() {
		return $this->x;
	}

	public function getY() {
		return $this->y;
	}

	public function setX($float) {
		$this->x = (float) ($float >= -180 && $float <= 180) ? $float : null;
	}

	public function setY($float) {
		$this->y = (float) ($float >= -90 && $float <= 90) ? $float : null;
	}

}

?>
