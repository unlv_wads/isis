<?php
/*****************************************************************************
Copyright © 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

class TileView extends ResultsView implements iResultsView, iResultsHTMLView {

	public function getFormattedResults(SpatialSearch $s) {
		$results = $s->getResults();
		$fields = Preference::getInstance()->getDisplayFields();

		// assemble table body
		$max = min(array($this->getResultsPerPage(), sizeof($results)));

		$i = 0; $rows = array();
		$numrows = Preference::getInstance()->getTileRows();
		$numcols = Preference::getInstance()->getTileColumns();
		for ($r = 0; $r < $numrows; $r++) {
			$cols = array();
			for ($c = 0; $c < $numcols; $c++) {
				if ($i >= $max) break;

				// collection grouping
				$cid = $results[$i]->getCollectionID();
				if ($s->isGroupingByCollection() && $cid <> $last_cid) {
					$cols[] = sprintf('<tr><td class="colGroupDivider" colspan="%d">
							<strong><a href="%s">%s</a></strong>
							<br/>(<a href="%s">%s</a>)</td></tr>',
						count($fields),
						String::websafe($results[$i]->getCollection()->getIntroURL()),
						String::websafe($results[$i]->getCollection()->getName()),
						String::websafe($results[$i]->getCollection()->getOrganizationURL()),
						String::websafe($results[$i]->getCollection()->getOrganization())
					);
				}

				try {
					$cols[] = sprintf('<td style="text-align:center"><a href="%s"><img src="%s"
							alt="map thumbnail" class="resultsThumb"
							onmouseover="HighlightMap(\'%s\'); return false; "/>
							<br/>%d) %s</a></td>',
						String::websafe($results[$i]->getRecordURL()),
						String::websafe($results[$i]->getThumbURL()),
						$results[$i]->getCollection()->getID() . '-'
							. $results[$i]->getPtr(),
						1,
						String::truncate(
							String::websafe($results[$i]->getMetadata('dc:title')),
							Preference::getInstance()->getGridMaxWords())
					);
				}
				catch (UnavailableMapException $e) {
					$cols[] = '<td>Unavailable map</td>';
				}
				$i++;
				$last_cid = $cid;
			} // for
			$rows[] = '<tr>' . implode('', $cols) . '</tr>';
		} // for
		$body = implode('', $rows);

		// return assembled table
		return '<table cellpadding="0" cellspacing="0" border="0"
			id="resultsGridTable">' . $body . '</table>';
	}


	public function getResultsPerPage() {
		return Preference::getInstance()->getTileRows()
			* Preference::getInstance()->getTileColumns();
	}

} // TileView

?>
