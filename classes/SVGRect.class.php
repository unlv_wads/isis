<?php
/*****************************************************************************
Copyright © 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

interface iSVGRect {
	function getHeight();
	function setHeight($float);
	function getTag();
	function getWidth();
	function setWidth($float);
	function getX();
	function setX($float);
	function getY();
	function setY($float);
}

// Class for handling SVG <rect> elements
class SVGRect implements iSVGRect {

	private $x, $y, $width, $height;


	public function __construct($x, $y, $width, $height) {
		$this->setHeight($height);
		$this->setWidth($width);
		$this->setX($x);
		$this->setY($y);
	}

	public function getHeight() {
		return $this->height;
	}

	public function setHeight($float) {
		$this->height = abs($float);
	}

	public function getTag($id=null) {
	/* Returns 1-2 closed element tags for the current rect with id, x, y,
		width, height attributes. 2 tags in the case of rects spanning the
		prime meridian. */
		$tag = '<rect ';
		if (!empty($id)) $tag .= sprintf('id="%s" ', $id);

		if ($this->getX() + $this->getWidth() <= 180) {
			$tag1 .= sprintf('%s x="%f" y="%f" width="%f" height="%f" />',
				$tag, $this->getX(), $this->getY(),
				$this->getWidth(), $this->getHeight()
			);
		}
		// If the rect spans the prime meridian, return two tags
		else {
			$width1 = 180 - $this->getX();
			$width2 = $this->getWidth() - 180 - $this->getX();
			$tag1 .= "$tag x=\"{$this->getX()}\" y=\"{$this->getY()}\"
				width=\"$width1\" height=\"{$this->get_height()}\"/>\n";
			$tag2 .= "$tag x=\"0\" y=\"{$this->get_y()}\"
				width=\"$width2\"	height=\"{$this->get_height()}\"/>\n";
		}

		return $tag1 . $tag2;
	}

	public function getWidth() {
	/* note: this is the *total width* of the rect even if it happens to span
	the prime meridian, which would divide it into 2 rects for display. */
		return $this->width;
	}

	public function setWidth($float) {
		$this->width = abs($float);
	}

	public function getX() {
		return $this->x;
	}
	public function getY() {
		return $this->y;
	}

	public function setX($float) {
		$this->x = $float;
	}

	public function setY($float) {
		$this->y = $float;
	}

} // SVGRect

?>
