<?php
/*****************************************************************************
Copyright © 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

interface iSpatialSearch {
	function isAscending();
	function setAscending($bool);
	function getCollectionIDs();
	function setCollectionIDs(array $ids);
	function getCollectionsInResults();
	function isGroupingByCollection();
	function setGroupingByCollection($bool);
	function getNumResults();
	function getPage();
	function setPage($int);
	function getResults();
	function setResults(array $r);
	function getResultIndex();
	function getSort();
	function setSort($sort);
}

class SpatialSearch implements iSpatialSearch {

	private $num_results, $page, $ascending;
	private $collection_ids = array();
	private $sort = 'id';
	private $collection_grouping = false;
	private $view; 	// ResultsView object
	private $results = array();
	public static $valid_sorts = array(
		'area' => 'Area',
		'clat' => 'Center Lat',
		'clong' => 'Center Long',
		'ptr' => 'CISOPTR'
	);


	public function __construct(ResultsView $view) {
		$this->setView($view);
	}


	public function searchAll() {
		$this->setCollectionsInResults(Collection::getAll());
		$this->setNumResults(Map::getNumAll());
		$this->setResults(
			Map::getAll($this->getPage(), $this->getView()->getResultsPerPage(),
				$this->getSort(), $this->getSortOrder())
		);
	}


	public final function isAscending() {
		return $this->ascending;
	}


	public final function setAscending($bool) {
		$this->ascending = ($bool) ? true : false;
	}


	public function getCollectionIDs() {
		return $this->collection_ids;
	}


	public function setCollectionIDs(array $c) {
		$this->collection_ids = $c;
	}


	public function getCollectionsInResults() {
		return $this->collections_in_results;
	}


	private function setCollectionsInResults(array $c) {
		$this->collections_in_results = $c;
	}


	public final function isGroupingByCollection() {
		return $this->collection_grouping;
	}


	public final function setGroupingByCollection($bool) {
		$this->collection_grouping = ($bool) ? true : false;
	}


	public final function getNumResults() {
		return $this->num_results;
	}


	private function setNumResults($int) {
		$this->num_results = $int;
	}


	public function searchOutside($long_n, $long_s, $long_e, $long_w) {
		$this->setCollectionsInResults(
			Collection::getAllOutside($long_n, $long_s, $long_e, $long_w)
		);
		$this->setNumResults(
			DataStore::getInstance()->getNumMapsOutside(
				$long_n, $long_s, $long_e, $long_w, $this->getCollectionIDs())
		);
		$this->setResults(
			DataStore::getInstance()->getAllMapsOutside(
				$long_n, $long_s, $long_e, $long_w,
				$this->getPage(), $this->getView()->getResultsPerPage(),
				$this->getSort(), $this->getSortOrder(),
				$this->isGroupingByCollection(), $this->getCollectionIDs())
		);
	}


	public final function getPage() {
		return $this->page;
	}


	public final function setPage($int) {
		$this->page
			= ($int > 0 && $int < 99999 && round($int) == $int) ? $int : 1;
	}


	public function searchPartiallyWithin($long_n, $long_s, $long_e, $long_w) {
		$this->setCollectionsInResults(
			Collection::getAllPartiallyWithin($long_n, $long_s, $long_e, $long_w)
		);
		$this->setNumResults(
			DataStore::getInstance()->getNumMapsPartiallyWithin(
				$long_n, $long_s, $long_e, $long_w, $this->getCollectionIDs())
		);
		$this->setResults(
			DataStore::getInstance()->getAllMapsPartiallyWithin(
				$long_n, $long_s, $long_e, $long_w,
				$this->getPage(), $this->getView()->getResultsPerPage(),
				$this->getSort(), $this->getSortOrder(),
				$this->isGroupingByCollection(), $this->getCollectionIDs())
		);
	}


	public final function getResults() {
		return $this->results;
	}


	public final function setResults(array $r) {
		$this->results = $r;
	}


	public function getResultIndex() {
		return ($this->getPage() - 1) * $this->getView()->getResultsPerPage() + 1;
	}


	public final function getSort() {
		return $this->sort;
	}


	public final function setSort($sort) {
		$this->sort = (in_array($sort, array_keys(self::$valid_sorts)))
			? $sort : 'area';
	}


	public final function getSortOrder() {
		return ($this->ascending) ? 'asc' : 'desc';
	}


	public function searchWithin($lat_n, $lat_s, $long_e, $long_w) {
		$this->setCollectionsInResults(
			Collection::getAllWithin($lat_n, $lat_s, $long_e, $long_w)
		);
		$this->setNumResults(
			DataStore::getInstance()->getNumMapsWithin(
				$lat_n, $lat_s, $long_e, $long_w, $this->getCollectionIDs())
		);
		$this->setResults(
			DataStore::getInstance()->getAllMapsWithin(
				$lat_n, $lat_s, $long_e, $long_w,
				$this->getPage(), $this->getView()->getResultsPerPage(),
				$this->getSort(), $this->getSortOrder(),
				$this->isGroupingByCollection(), $this->getCollectionIDs())
		);
	}


	public function getView() {
		return $this->view;
	}


	private function setView(ResultsView $v) {
		$this->view = $v;
	}

} // SpatialSearch

?>
