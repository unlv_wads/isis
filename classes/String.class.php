<?php
/*****************************************************************************
Copyright © 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

class String {

	const COLLECTION_ALREADY_EXISTS = 'This collection already exists in the
		data store.';
	const COLLECTION_DOES_NOT_EXIST_IN_CDM = 'This collection does not exist
		on the CONTENTdm&reg; server. Make sure the alias has been entered
		correctly and try again.';
	const COLLECTION_SAVED = 'Collection saved successfully.';
	const COLLECTIONS_DELETED = 'Collection(s) deleted successfully.';
	const DBMS_CONNECT_FAILURE = 'Unable to connect to the %s server. Make
		sure the server is running and accessible and that the supplied username
		and password are valid.';
	const DBMS_NOT_COMPILED_INTO_PHP = 'The %s database engine has not been
		compiled into the version of PHP running on this server.';
	const ERROR_DELETING_COLLECTIONS = 'Error deleting collection(s).';
	const ERROR_DELETING_MAPS = 'Error deleting map(s).';
	const INVALID_COLLECTION = 'The specified collection does not exist in
		the data store.';
	const INVALID_COORDINATE_INEQUALITY = 'Invalid coordinate; north latitude
		must always be greater than south latitude and west longitude must always
		be greater than east longitude.';
	const INVALID_DBMS = 'The chosen database engine is not available.';
	const INVALID_LAT_N = 'Northern latitude must be greater than -90&deg; and
		less than or equal to 90&deg;.';
	const INVALID_LAT_S = 'Southern latitude must be greater than or equal to
		-90&deg; and less than 90&deg;.';
	const INVALID_LONG_E = 'Eastern longitude must be greater than -180&deg; and
		less than or equal to 180&deg;.';
	const INVALID_LONG_W = 'Western longitude must be greater than or equal to
		-180&deg; and less than 180&deg;.';
	const INVALID_RELATIVE_LAT = 'Northern latitude must be numerically
		greater than southern latitude.';
	const INVALID_RELATIVE_LONG = 'Eastern longitude must be numerically
		greater than western longitude.';
	const INVALID_OAI_ID = 'Invalid OAI ID: ';
	const INVALID_STORAGE_ENGINE = 'The chosen storage engine is not available.';
	const LOAD_DATA_XML_FAILED = 'Unable to open the data.xml file. Make sure
		this file exists and is readable by the web server, or else choose a
		different data store in the control panel.';
	const LOGIN_FAILED = 'Login failed.';
	const MAP_ALREADY_EXISTS = 'This map has already been entered into the
		index.';
	const MAP_DOES_NOT_EXIST_IN_CDM = 'This map does not exist on the
		CONTENTdm&reg; server. Double-check the pointer and collection
		and try again.';
	const MAP_DOES_NOT_EXIST_IN_DATA_STORE = 'This map does not exist in the
		data store.';
	const MAP_SAVED = 'Map saved successfully.';
	const MAPS_DELETED = 'Map(s) deleted successfully.';
	const MISSING_CONFIGXML_FILE = 'Unable to open the config.xml.php file. This
		must be a valid XML file located in the ISIS root folder.';
	const MISSING_MAP_INFO = 'You must fill in all fields when adding a new
		map.';
	const MISSING_USERNAME_OR_PASS = 'Both username and password are required.';
	const NO_RESULTS = 'No maps found for your search. Try moving or enlarging
		your selection, or changing your search type.';
	const SAVE_DATA_XML_FAILED = 'Unable to save the data.xml file. Make sure
		the web server has the necessary permissions to write to the file.';
	const SAVE_PREFS_FAILED = 'Unable to save the preferences. Make sure
		the web server has the necessary permissions to write to the
		config.xml.php file.';
	const SETTINGS_SAVED = 'Settings have been saved successfully.';
	const NO_OAI_RESPONSE = 'No OAI-PMH response from %s. Check the
		"base URL" and "path to oai.exe" and try again.';
	const USER_LOGGED_OUT = 'You have been logged out. Have a nice day.';


	public static function hyperlink($text) {
	// detect & hyperlink URLs in $text
		$tmp = explode(' ', $text);
		foreach ($tmp as &$v) {
			if (in_array(substr($v, 0, 7), array('http://', 'https:/')))
				$v = "<a href=\"$v\">$v</a>";
		}
		return implode(' ', $tmp);
	} // hyperlink()


	public static final function paranoid($string, $allowed = array()) {
	// this method stolen from CakePHP
		$allow = null;
		if (!empty($allowed)) {
			foreach ($allowed as $value) {
				$allow .= "\\$value";
			}
		}

		if (is_array($string)) {
			$cleaned = array();
			foreach ($string as $key => $clean) {
				$cleaned[$key] = preg_replace("/[^{$allow}a-zA-Z0-9]/", '', $clean);
			}
		} else {
			$cleaned = preg_replace("/[^{$allow}a-zA-Z0-9]/", '', $string);
		}
		return $cleaned;
	} // paranoid()


	public static function truncate($str, $max_words) {
		$text_array = explode(' ', $str);
		if (count($text_array) > $max_words && $max_words > 0) {
			$str = implode(' ', array_slice($text_array, 0, $max_words))
				. '...';
		}
		return $str;
	} // truncate()


	public static function unMagicQuotes(&$array_or_string) {
		if (get_magic_quotes_gpc()) {
			if (is_array($array_or_string)) {
				foreach ($array_or_string as &$value) {
					if (!is_array($value)) {
						$value = stripslashes($value);
					}
					else self::unMagicQuotes($value);
				}
			}
		}
	} // unMagicQuotes()


	public static function websafe($str) {
		return htmlspecialchars(strip_tags(trim($str)), ENT_QUOTES, 'UTF-8');
	} // websafe()


	public static function xmlentities($string) {
		return str_replace(array('&', '"', "'", '<', '>', '’' ),
			array('&amp;', '&quot;', '&apos;', '&lt;', '&gt;', '&apos;'),
			$string);
	} // xmlentities()

} // String

?>
