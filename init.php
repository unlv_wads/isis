<?php
/*****************************************************************************
Copyright © 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

class Config {

	const ISIS_FOLDER_ROOT = '/isis';

}

function __autoload($class) {
	$errmsg = sprintf('Unable to load the %s class file. Make sure the file
		is readable by the web server and that ISIS_FOLDER_ROOT has
		been set properly in init.php.', $class);
	if (strpos($class, 'Controller') !== false) {
		if (!@include_once($_SERVER['DOCUMENT_ROOT'] . '/'
		. trim(Config::ISIS_FOLDER_ROOT, '/')
		. '/controllers/' . $class . '.class.php')) {
			die($errmsg);
		}
	}
	else if (strpos($class, 'Exception') !== false) {
		if (!@include_once($_SERVER['DOCUMENT_ROOT'] . '/'
		. trim(Config::ISIS_FOLDER_ROOT, '/')
		. '/classes/CustomException.class.php')) {
			die($errmsg);
		}
	}
	else {
		if (!@include_once($_SERVER['DOCUMENT_ROOT'] . '/'
			. trim(Config::ISIS_FOLDER_ROOT, '/')
			. '/classes/' . $class . '.class.php')) {
			die($errmsg);
		}
	}
}

session_name('isis');
session_start();

try {
	if (Preference::getInstance()->isDebugMode()) {
		error_reporting(E_ALL);
	}
}
catch (PreferencesException $e) {
	die($e->getMessage());
}

?>
