<?php
/*****************************************************************************
Copyright © 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

require_once('../init.php');
header('Content-type: text/javascript');
?>

/*
SVG rect drag & resize
Alex Dolski 2008/02
*/

var draggingElement = null;
var mouseOffsetX = 0;
var mouseOffsetY = 0;


function mouseDown(evt) {
	var target = evt.currentTarget;
	draggingElement = target;

	if (target) {
		var p = document.documentElement.createSVGPoint();
		p.x = evt.clientX;
		p.y = evt.clientY;

		var m = getScreenCTM(document.documentElement);

		p = p.matrixTransform(m.inverse());
		mouseOffsetX = p.x - parseFloat(target.getAttribute("dragx"));
		mouseOffsetY = p.y - parseFloat(target.getAttribute("dragy"));
	}
}


function mouseUp(evt) {
	draggingElement = null;
	mouseOffsetX = 0;
	mouseOffsetY = 0;
}


function mouseMove(evt) {
	var p = document.documentElement.createSVGPoint();
	p.x = evt.clientX;
	p.y = evt.clientY;

	var m = getScreenCTM(document.documentElement);

	p = p.matrixTransform(m.inverse());
	p.x -= mouseOffsetX;
	p.y -= mouseOffsetY;

	var f = document.getElementById("frame");
	var fx = parseFloat(f.getAttribute("x"));
	var fy = parseFloat(f.getAttribute("y"));
	var fw = parseFloat(f.getAttribute("width"));
	var fh = parseFloat(f.getAttribute("height"));

	var r = document.getElementById("resizer");
	var rx = parseFloat(r.getAttribute("x"));
	var ry = parseFloat(r.getAttribute("y"));

	switch (draggingElement) {
		case f:
			f.setAttribute("x", p.x);
			f.setAttribute("y", p.y);
			f.setAttribute("dragx", p.x);
			f.setAttribute("dragy", p.y);

			r.setAttribute("x", p.x + fw - <?= Preference::getInstance()->getResizerThickness() ?>);
			r.setAttribute("y", p.y + fh - <?= Preference::getInstance()->getResizerThickness() ?>);
			r.setAttribute("dragx",
				p.x + fw - <?= Preference::getInstance()->getResizerThickness() ?>);
			r.setAttribute("dragy",
				p.y + fh - <?= Preference::getInstance()->getResizerThickness() ?>);
			break;
		case r:
			// enforce minimum width
			if (p.x > fx) {
				r.setAttribute("x", p.x);
				r.setAttribute("dragx", p.x);
				f.setAttribute("width",
					rx - fx + <?= Preference::getInstance()->getResizerThickness() ?>);
			}
			else {
				r.setAttribute("x", fx);
				r.setAttribute("dragx", fx);
				f.setAttribute("width", <?= Preference::getInstance()->getResizerThickness() ?>);
			}
			// enforce minimum height
			if (p.y > fy) {
				r.setAttribute("y", p.y);
				r.setAttribute("dragy", p.y);
				f.setAttribute("height",
					ry - fy + <?= Preference::getInstance()->getResizerThickness() ?>);
			}
			else {
				r.setAttribute("y", fy);
				r.setAttribute("dragy", fy);
				f.setAttribute("height", <?= Preference::getInstance()->getResizerThickness() ?>);
			}
			break;
	} // switch (draggingElement)

	var x1 = fx;
	var y1 = fy + fh;
	var x2 = x1 + fw;
	var y2 = fy;

	// Correct for SVG's upside-down coordinate system
	y1 = (y1 < 0) ? -y1 : y1;
	y2 = (y2 < 0) ? -y2 : y2;

	x1 = x1.toFixed(<?= Preference::getInstance()->getNumMapCoordinateDigits() ?>);
	x2 = x2.toFixed(<?= Preference::getInstance()->getNumMapCoordinateDigits() ?>);
	y1 = y1.toFixed(<?= Preference::getInstance()->getNumMapCoordinateDigits() ?>);
	y2 = y2.toFixed(<?= Preference::getInstance()->getNumMapCoordinateDigits() ?>);

	displaySelection(x1, y1, x2, y2); // displaySelection is in functions.js
}



function init() {
	var f = document.getElementById("frame");
	var r = document.getElementById("resizer");

	if (f && r) {
		f.addEventListener("mousedown", mouseDown, false);
		r.addEventListener("mousedown", mouseDown, false);
	}
}
