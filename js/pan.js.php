<?php
/*****************************************************************************
Copyright © 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

require_once('../init.php');
header('Content-type: text/javascript');
?>

/*
SVG rectangle drag
Alex Dolski 2007/12
*/

var enforceBounds = <?php if (Preference::getInstance()->shouldEnforcePanMapBounds()) echo 'true';
	else echo 'false'; ?>;

var mouseDragging = false;
var mouseOffsetX = 0;
var mouseOffsetY = 0;


function mouseDown(evt) {
	mouseDragging = true;

	var frame = document.getElementById("pannedArea");
	if (frame) {
		var p = document.documentElement.createSVGPoint();
		p.x = evt.clientX;
		p.y = evt.clientY;

		var m = getScreenCTM(document.documentElement);
		p = p.matrixTransform(m.inverse());
		mouseOffsetX = p.x - parseFloat(frame.getAttribute("x"));
		mouseOffsetY = p.y - parseFloat(frame.getAttribute("y"));
	}
}

function mouseUp(evt) {
	mouseDragging = false;
	mouseOffsetX = 0;
	mouseOffsetY = 0;
} // mouseUp(evt)


function mouseMove(evt) {
	var p = document.documentElement.createSVGPoint();
	p.x = evt.clientX;
	p.y = evt.clientY;

	if (mouseDragging) {
		var vbForm = parent.document.getElementById("vb"); // search_div.inc.php
		var mapEmbed = parent.document.getElementById("svgMap").getSVGDocument();
		var panSVG = document.getElementById("panSVG"); // pan.svg.php
		var mapSVG = mapEmbed.getElementById("rootSVG"); // drag.svg.php
		var frame = document.getElementById("pannedArea"); // pan.svg.php

		var viewBoxArray = panSVG.getAttribute("viewBox").split(" ");
		var vbx = parseFloat(viewBoxArray[0]);
		var vby = parseFloat(viewBoxArray[1]);
		var vbw = parseFloat(viewBoxArray[2]);
		var vbh = parseFloat(viewBoxArray[3]);

		if (frame) {
			var m = getScreenCTM(document.documentElement);
			p = p.matrixTransform(m.inverse());

			var mx = parseFloat(p.x - mouseOffsetX);
			var my = parseFloat(p.y - mouseOffsetY);

			var fx = parseFloat(frame.getAttribute("x"));
			var fy = parseFloat(frame.getAttribute("y"));
			var fw = parseFloat(frame.getAttribute("width"));
			var fh = parseFloat(frame.getAttribute("height"));

			if (enforceBounds) {
				// prevent too far left
				if (fx < vbx) fx = vbx;
				else if (mx < vbx) fx = vbx;
				else fx = mx;
				// prevent too far up
				if (fy < vby) fy = vby;
				else if (my < vby) fy = vby;
				else fy = my;
				// prevent too far right
				if (fx + fw > vbx + vbw) fx = vbx + vbw - fw;
				// prevent too far down
				if (fy + fh > vby + vbh) fy = vby + vbh - fh;

				var vb = fx + " " + fy + " " + fw + " " + fh;
			}
			else {
				fx = mx;
				fy = my;
				var vb = mx + " " + my + " " + fw + " " + fh;
			}

			frame.setAttribute("x", fx);
			frame.setAttribute("y", fy);
			mapSVG.setAttribute("viewBox", vb);
			vbForm.setAttribute("value", vb);
		} // if (frame)

		var x1 = fx;
		var y1 = fy;
		var x2 = fx + fw;
		var y2 = fy + fh;

		// Correct for SVG's upside-down coordinate system relative to Cartesian coords
		y1 = (y1 < 0) ? -y1 : y1;
		y2 = (y2 < 0) ? -y2 : y2;
		// displayCoverage() is in functions.js
		displayCoverage(x1, y1, x2, y2, <?= Preference::getInstance()->getNumMapCoordinateDigits() ?>);

	} // if (mouseDragging)
}

function init() {
	var frame = document.getElementById("pannedArea");

	if (frame) {
		frame.addEventListener("mousedown", mouseDown, false);
		frame.addEventListener("mouseup", mouseUp, false);
		frame.addEventListener("mousemove", mouseMove, false);
	}
}
