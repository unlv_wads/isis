<?php
/*****************************************************************************
Copyright � 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

class CollectionController extends Controller {

	private $collection;	// Collection object
	protected $validation_rules = array(
		'alias' => array(
			'name' => 'Alias',
			'required' => true,
			'maxlength' => 50,
			'min' => null,
			'max' => null,
			'allowed_values' => null
		),
		'base_url' => array(
			'name' => 'Base URL',
			'required' => true,
			'maxlength' => 50,
			'min' => null,
			'max' => null,
			'allowed_values' => null
		),
		'intro_url' => array(
			'name' => 'Introductory URL',
			'required' => false,
			'maxlength' => 200,
			'min' => null,
			'max' => null,
			'allowed_values' => null
		),
		'name' => array(
			'name' => 'Name',
			'required' => true,
			'maxlength' => 200,
			'min' => null,
			'max' => null,
			'allowed_values' => null
		),
		'oai_id_prefix' => array(
			'name' => 'OAI ID prefix',
			'required' => true,
			'maxlength' => 100,
			'min' => null,
			'max' => null,
			'allowed_values' => null
		),
		'organization' => array(
			'name' => 'Organization',
			'required' => true,
			'maxlength' => 500,
			'min' => null,
			'max' => null,
			'allowed_values' => null
		),
		'org_url' => array(
			'name' => 'Organization URL',
			'required' => false,
			'maxlength' => 1000,
			'min' => null,
			'max' => null,
			'allowed_values' => null
		),
		'path_to_oai' => array(
			'name' => 'Path to oai.exe',
			'required' => true,
			'maxlength' => 50,
			'min' => null,
			'max' => null,
			'allowed_values' => null
		),
		'path_to_thumbnail' => array(
			'name' => 'Path to thumbnail.exe',
			'required' => true,
			'maxlength' => 50,
			'min' => null,
			'max' => null,
			'allowed_values' => null
		)
	);


	public function __construct() {
		parent::__construct();
	}


	public function add() {
		if (!User::isLoggedIn()) {
			self::redirect('../login.php');
		}

		$this->setCollection(new Collection);

		if (!array_key_exists('add', $_POST)) return false;

		$this->setCollection($this->instantiateFromFormData($_POST));

		if (!$this->validate($_POST)) return false;

		try {
			if ($this->getCollection()->hasSiblingWithSameNameAndURLInDataStore()) {
				self::setFlash(String::COLLECTION_ALREADY_EXISTS);
				return false;
			}
			else if (!$this->getCollection()->existsInCONTENTdm()) {
				self::setFlash(String::COLLECTION_DOES_NOT_EXIST_IN_CDM);
				return false;
			}
			if ($this->getCollection()->save()) {
				self::setFlash(String::COLLECTION_SAVED);
				self::redirect('.');
			}
		}
		catch (UnknownHostException $e) {
			self::setFlash($e->getMessage());
		}
		catch (DataStoreException $e) {
			self::setFlash($e->getMessage());
		}
	}


	public function delete(array $ids) {
		if (!User::isLoggedIn()) {
			self::redirect('../login.php');
		}

		foreach ($ids as $id) {
			if ($id <> round($id) || $id < 0) continue;
			$col = new Collection((int) $id, false);
			try {
				if (!$col->delete()) {
					self::setFlash(String::ERROR_DELETING_COLLECTIONS);
					return false;
				}
				else self::setFlash(String::COLLECTIONS_DELETED);
			}
			catch (DataStoreException $e) {
				self::setFlash($e->getMessage());
			}
		}
	}


	public function edit($id) {
		if (!User::isLoggedIn()) {
			self::redirect('../login.php');
		}
		$id = (int) String::paranoid($id);

		if (!array_key_exists('edit', $_POST)) {
			$this->setCollection(new Collection($id));
			return false;
		}

		$this->setCollection(
			$this->instantiateFromFormData($_POST, $id));

		if (!$this->validate($_POST)) return false;

		if (!$this->getCollection()->hasSiblingWithSameNameAndURLInDataStore()) {
			self::setFlash(String::INVALID_COLLECTION); return false;
		}
		else if (!$this->getCollection()->existsInCONTENTdm()) {
			self::setFlash(String::COLLECTION_DOES_NOT_EXIST_IN_CDM); return false;
		}

		try {
			if ($this->getCollection()->save()) {
				self::setFlash(String::COLLECTION_SAVED);
			}
		}
		catch (DataStoreException $e) {
			self::setFlash($e->getMessage());
		}

	}


	public function index() {
		if (!User::isLoggedIn()) {
			self::redirect('../login.php');
		}
	}


	private function instantiateFromFormData(array $data, $id = null) {
		$c = (is_int($id) && $id >= 0) ? new Collection($id) : new Collection;
		$c->setAlias((string) $data['alias']);
		$c->setBaseURL((string) $data['base_url']);
		$c->setIntroURL((string) $data['intro_url']);
		$c->setName((string) $data['name']);
		$c->setOAIIDPrefix((string) $data['oai_id_prefix']);
		$c->setOrganization((string) $data['organization']);
		$c->setOrganizationURL((string) $data['org_url']);
		$c->setPathToOAI((string) $data['path_to_oai']);
		$c->setPathToThumbnail((string) $data['path_to_thumbnail']);
		return $c;
	}


	public final function getCollection() {
		return $this->collection;
	}


	protected final function setCollection(Collection $c) {
		$this->collection = $c;
	}

} // CollectionController

?>
