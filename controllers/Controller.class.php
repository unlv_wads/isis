<?php
/*****************************************************************************
Copyright � 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

class Controller {

	public function __construct() {
		String::unMagicQuotes($_GET);
		String::unMagicQuotes($_POST);
		$this->checkSanity();
	}


	protected function checkSanity() {
		try {
			// make sure the prefs are available
			$p = Preference::getInstance();
			// run a simple data store accessor to test the data store
			// (DataStore::isAvailable() will catch exceptions)
			$result = Collection::getAll();
		}
		catch (PreferencesException $e) {
			CustomException::add($e);
			require_once('error.php'); die;
		}
		catch (DataStoreException $e) {
			CustomException::add($e);
			require_once('error.php'); die;
		}
	}


	protected static final function setFlash($str) {
		$_SESSION['flash'] = $str;
	}


	protected static final function redirect($url) {
		header('Location: ' . $url); die;
	}


	protected function validate(array $data) {
		foreach ($this->validation_rules as $key => $attrs) {
			if (!array_key_exists($key, $data)) continue;
			if (is_array($data[$key])) {
				foreach($data[$key] as $value) {
					self::validateValue($attrs, $value);
				}
			}
			else self::validateValue($attrs, $data[$key]);
		}
		return true;
	} // validate()


	protected static function validateValue(array $rule, $value) {
	// This method should be run only by validate()
		// check required
		if ($rule['required']) {
			if (empty($value)) {
				self::setFlash($rule['name'] . ' is required.');
				return false;
			}
		}
		// check allowed values
		if (is_array($rule['allowed_values'])) {
			if (!in_array($value, $rule['allowed_values'])) {
				self::setFlash($rule['name'] . ' must be one of: '
					. implode(' ', $rule['allowed_values']));
				return false;
			}
		}
		// check max length
		if (is_numeric($rule['maxlength'])) {
			if (strlen($value) > $rule['maxlength']) {
				self::setFlash($rule['name'] . ' must be no more than '
					. $rule['maxlength'] . ' characters.');
				return false;
			}
		}
		// check min & max
		else {
			if (is_numeric($rule['min']) && $value < $rule['min']) {
				self::setFlash($rule['name'] . ' must be greater than or
					equal to ' . $rule['min'] . ' characters.');
				return false;
			}
			else if (is_numeric($rule['max']) && $value > $rule['max']) {
				self::setFlash($rule['name'] . ' must be less than or
					equal to ' . $rule['max'] . ' characters.');
				return false;
			}
		}
	}

} // Controller

?>
