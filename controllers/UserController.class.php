<?php
/*****************************************************************************
Copyright � 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

class UserController extends Controller {

	private $user;	// User object


	public function __construct() {
		parent::__construct();
	}


	public function login() {
		if (strlen($_POST['username']) < 1 || strlen($_POST['password']) < 1) {
			self::setFlash(String::MISSING_USERNAME_OR_PASS);
		}
		else {
			$username = String::paranoid($_POST['username'], array('_', '.'));
			$password = String::paranoid($_POST['password'], array('_', '-', '.'));
			$user = new User($username);
			if ($user->login($password)) {
				self::redirect('index.php');
			}
			else {
				self::setFlash(String::LOGIN_FAILED);
			}
		}
	}


	public function logout() {
		$user = User::getInstance();
		if ($user instanceof User) $user->logout();
		self::setFlash(String::USER_LOGGED_OUT);
		self::redirect('login.php');
	}


	public final function getUser() {
		return $this->user;
	}


	protected final function setUser(User $u) {
		$this->user = $u;
	}

}

?>
