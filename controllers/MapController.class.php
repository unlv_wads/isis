<?php
/*****************************************************************************
Copyright � 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

class MapController extends Controller {

	private $map;					// Map object
	private $search;				// SpatialSearch object
	private $view;					// View child object


	public function __construct() {
		parent::__construct();
	}


	public function add() {
		if (!User::isLoggedIn()) {
			self::redirect('../login.php');
		}

		$this->setMap(new Map);

		if (!array_key_exists('add', $_POST)) return false;

		$this->setMap($this->instantiateFromFormData($_POST));

		if (!$this->validate($_POST)) return false;

		if ($this->getMap()->hasSiblingWithSameAliasPtrInDataStore()) {
			self::setFlash(String::MAP_ALREADY_EXISTS);
			return false;
		}
		else if (!$this->getMap()->existsInCONTENTdm()) {
			self::setFlash(String::MAP_DOES_NOT_EXIST_IN_CDM);
			return false;
		}

		try {
			if ($this->getMap()->save()) {
				self::setFlash(String::MAP_SAVED);
				self::redirect('.');
			}
		}
		catch (DataStoreException $e) {
			self::setFlash($e->getMessage());
		}
	}


	public function delete(array $ids) {
		if (!User::isLoggedIn()) {
			self::redirect('../login.php');
		}

		foreach ($ids as $id) {
			if ($id <> round($id) || $id < 0) continue;
			$map = new Map((int) $id, false);
			try {
				if (!$map->delete()) {
					self::setFlash(String::ERROR_DELETING_MAPS);
					return false;
				}
				else self::setFlash(String::MAPS_DELETED);
			}
			catch (DataStoreException $e) {
				self::setFlash($e->getMessage());
			}
		}
	}


	public function edit($id) {
		if (!User::isLoggedIn()) {
			self::redirect('../login.php');
		}
		$id = (int) String::paranoid($id);

		$this->setMap(new Map($id));

		if (!array_key_exists('edit', $_POST)) return false;


		if (!$this->validate($_POST)) return false;
		$updated_map = $this->instantiateFromFormData($_POST, $id);

		if (!$updated_map->hasSiblingWithSameIDInDataStore()) {
			self::setFlash(String::MAP_DOES_NOT_EXIST_IN_DATA_STORE); return false;
		}

		$this->setMap($updated_map);

		try {
			if ($this->getMap()->save()) {
				self::setFlash(String::MAP_SAVED);
			}
		}
		catch (UnknownHostException $e) {
			self::setFlash($e->getMessage());
		}
		catch (DataStoreException $e) {
			self::setFlash($e->getMessage());
		}
	}


	public function indexAdminView() {
		if (!User::isLoggedIn()) {
			self::redirect('../login.php');
		}
		$this->setView(new AdminMapsView);
		$this->setSearch(new SpatialSearch($this->getView()));
		$this->getSearch()->setPage($_GET['page']);
		$this->getSearch()->setAscending(($_GET['order'] == 'desc') ? false : true);
		try {
			$this->getSearch()->searchAll();
		}
		catch (PreferencesException $e) {
			CustomException::add($e);
			require_once('../error.php');
			die;
		}
		catch (DataStoreException $e) {
			CustomException::add($e);
			require_once('../error.php');
			die;
		}
	}


	public function indexMainView() {
	}


	public function indexMainSVGView() {
		$this->setView(new SVGView(
			Preference::getInstance()->getInitialViewX(),
			Preference::getInstance()->getInitialViewY(),
			Preference::getInstance()->getInitialViewWidth(),
			Preference::getInstance()->getInitialViewHeight())
		);

		if (!empty($_GET['lw']) && !empty($_GET['le'])
		&& !empty($_GET['ls']) && !empty($_GET['ln'])) {
			$fx = $_GET['lw'];
			$fy = -$_GET['ln'];
			$fw = $_GET['le'] - $_GET['lw'];
			$fh = $_GET['ln'] - $_GET['ls'];
		}
		else {
			$fx = Preference::getInstance()->getDefaultFrameX();
			$fy = Preference::getInstance()->getDefaultFrameY();
			$fw = Preference::getInstance()->getDefaultFrameWidth();
			$fh = Preference::getInstance()->getDefaultFrameHeight();
		}
		$this->getView()->setFrame(new SVGFrame($fx, $fy, $fw, $fh));
		$this->getView()->setResizer(new SVGResizer($this->getView()->getFrame()));
	}


	public function indexPanSVGView() {
		$this->setView(new SVGView(
			Preference::getInstance()->getMapX(),
			Preference::getInstance()->getMapY(),
			Preference::getInstance()->getMapWidth(),
			Preference::getInstance()->getMapHeight())
		);
	}


	public function indexResultsHTMLView() {
		// save collection search settings from back button obliteration
		if (array_key_exists('collection_ids', $_GET)) {
			if (is_array($_GET['collection_ids'])) {
				$_SESSION['collection_ids'] = array();
				foreach ($_GET['collection_ids'] as $int) {
					$_SESSION['collection_ids'][] = (int) $int;
				}
			}
		}

		switch ($_GET['view']) {
			case 'tile': $this->setView(new TileView); break;
			default: $this->setView(new GridView); break;
		}

		$cids = array_key_exists('collection_ids', $_GET)
			? $_GET['collection_ids'] : array();
		$this->setSearch(new SpatialSearch($this->getView()));
		$this->getSearch()->setPage($_GET['page']);
		$this->getSearch()->setSort($_GET['sort']);
		$this->getSearch()->setCollectionIDs($cids);
		$this->getSearch()->setAscending(
			($_GET['order'] == 'desc') ? false : true);
		$this->getSearch()->setGroupingByCollection(
			($_GET['group'] == 'true') ? true : false);

		try {
			switch ($_GET['search']) {
				case 'o':
					$this->getSearch()->searchOutside($_GET['ln'], $_GET['ls'],
						$_GET['le'], $_GET['lw']);
					break;
				case 'p':
					$this->getSearch()->searchPartiallyWithin($_GET['ln'],
						$_GET['ls'], $_GET['le'], $_GET['lw']);
					break;
				default:
					$this->getSearch()->searchWithin($_GET['ln'], $_GET['ls'],
						$_GET['le'], $_GET['lw']);
					break;
			}
		}
		catch (PreferencesException $e) {
			CustomException::add($e);
			require_once('./error.php');
			die;
		}
		catch (DataStoreException $e) {
			CustomException::add($e);
			require_once('./error.php');
			die;
		}

		if (sizeof($this->getSearch()->getResults()) < 1) {
			self::setFlash(String::NO_RESULTS);
			self::redirect('./index.php');
		}
	}


	public function indexResultsSVGView() {
		/* For "within" searches, we ignore the form-supplied viewbox and
		  calculate it based on the frame dimensions instead */
		if ($_GET['search'] == 'w'
		&& !empty($_GET['lw']) && !empty($_GET['le'])
		&& !empty($_GET['ls']) && !empty($_GET['ln'])) {
			$vx = $_GET['lw'] - 0.1;
			$vy = 0 - $_GET['ln'] - 0.1; // (inverted for svg canvas)
			$vw = $_GET['le'] - $_GET['lw'] + 0.2;
			$vh = $_GET['ln'] - $_GET['ls'] + 0.2;
		}
		// For "outside" searches, we want to show the whole map
		else if ($_GET['search'] == 'o') {
			$vx = Preference::getInstance()->getMapX();
			$vy = Preference::getInstance()->getMapY();
			$vw = Preference::getInstance()->getMapWidth();
			$vh = Preference::getInstance()->getMapHeight();
		}
		else if (!empty($_GET['vb'])) {
			list($vx, $vy, $vw, $vh) = explode(' ', $_GET['vb']);
		}

		$this->setView(new SVGView($vx, $vy, $vw, $vh));

		$cids = array_key_exists('collection_ids', $_GET)
			? $_GET['collection_ids'] : array();
		$this->setSearch(new SpatialSearch($this->getView()));
		$this->getSearch()->setCollectionIDs($cids);
		$this->getSearch()->setPage(1);
		try {
			switch ($_GET['search']) {
				case 'o':
					$this->getSearch()->searchOutside($_GET['ln'], $_GET['ls'],
						$_GET['le'], $_GET['lw']);
					break;
				case 'p':
					$this->getSearch()->searchPartiallyWithin($_GET['ln'],
						$_GET['ls'], $_GET['le'], $_GET['lw']);
					break;
				default:
					$this->getSearch()->searchWithin($_GET['ln'], $_GET['ls'],
						$_GET['le'], $_GET['lw']);
					break;
			}
		}
		catch (PreferencesException $e) {
			CustomException::add($e);
			require_once('./error.php');
			die;
		}
		catch (DataStoreException $e) {
			CustomException::add($e);
			require_once('./error.php');
			die;
		}
	}


	private function instantiateFromFormData(array $data, $id = null) {
		$m = (is_int($id) && $id >= 0) ? new Map($id) : new Map;
		$m->setCollectionID((int) $data['collection_id']);
		$m->setPtr((int) $data['ptr']);
		$m->setLatN((float) $data['ln']);
		$m->setLatS((float) $data['ls']);
		$m->setLongE((float) $data['le']);
		$m->setLongW((float) $data['lw']);
		return $m;
	}


	public final function getMap() {
		return $this->map;
	}


	protected final function setMap(Map $m) {
		$this->map = $m;
	}


	public final function getSearch() {
		return $this->search;
	}


	protected final function setSearch(SpatialSearch $s) {
		$this->search = $s;
	}


	public final function getView() {
		return $this->view;
	}


	protected final function setView(View $v) {
		$this->view = $v;
	}


	// override
	protected function validate(array $data) {
		if (empty($data['collection_id']) || $data['ptr'] < 0
		|| empty($data['ln']) || empty($data['ls'])
		|| empty($data['lw']) || empty($data['le'])) {
			self::setFlash(String::MISSING_MAP_INFO); return false;
		}
		else if ($data['ln'] <= $data['ls']) {
			self::setFlash(String::INVALID_RELATIVE_LAT); return false;
		}
		else if ($data['le'] <= $data['lw']) {
			self::setFlash(String::INVALID_RELATIVE_LONG); return false;
		}
		else if ($data['ln'] <= -90 || $data['ln'] > 90) {
			self::setFlash(String::INVALID_LAT_N); return false;
		}
		else if ($data['ls'] < -90 || $data['ls'] >= 90) {
			self::setFlash(String::INVALID_LAT_S); return false;
		}
		else if ($data['le'] <= -180 || $data['le'] > 180) {
			self::setFlash(String::INVALID_LONG_E); return false;
		}
		else if ($data['lw'] < -180 || $data['lw'] >= 180) {
			self::setFlash(String::INVALID_LONG_W); return false;
		}
		else {
			$col = new Collection($data['collection_id']);
			if (!$col->hasSiblingWithSameIDInDataStore()) {
				self::setFlash(String::INVALID_COLLECTION); return false;
			}
		}
		return true;
	} // validate()

} // MapController

?>
