<?
header("Content-type: image/svg+xml");
?>

<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
	"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg id="svgResults" width="100%" height="100%" version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink"
	preserveAspectRatio="xMidYMid meet"
	viewBox="0 0 200 200"
	style="background-color:white">

<text x="5" y="15" style="color:black; font-size:12px">
	<? foreach (CustomException::getAll() as $e) {
		echo String::xmlentities($e->getMessage()) . '; ';
	} ?>
</text>

</svg>
