<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title>ISIS @ UNLV: Search</title>

	<script type="text/javascript" src="./js/functions.js.php"></script>


	<? if (is_readable($_SERVER['DOCUMENT_ROOT'] . '/dc_styles/default.css')): ?>
		<link rel="stylesheet" type="text/css" media="screen"
			href="/dc_styles/default.css"/>
	<? else: ?>
		<link rel="stylesheet" type="text/css" media="screen"
			href="./templates/css/unlv.css"/>
	<? endif ?>
	<link rel="stylesheet" type="text/css" media="screen"
		href="./templates/css/main.css"/>

	<? include_once('./templates/css/browser_hacks.html'); ?>

</head>

<body onload="initEmbed('main')">

<? include_once('./templates/includes/header.html.php'); ?>

<div id="container">

	<div id="menu">
		<h1>Digital Collections</h1>
		<div class="navSubDiv" style="margin-bottom:50px">

			<embed id="svgPan" src="pan.svg.php" type="image/svg+xml"
				style="float:left"
				width="150" height="150"></embed>

			<table cellpadding="0" cellspacing="0" border="0"
				style="float:right; text-align:center; width:80px">
				<tr>
					<td style="width:33%"></td>
					<td style="width:33%"><h2 id="panUp"><a href="javascript:void(0);"
						onmousedown="ZoomAndPan('north'); return false;">&uarr;</a></h2></td>
					<td></td>
				</tr>
				<tr>
					<td><h2 id="panLeft"><a href="javascript:void(0);"
						onmousedown="ZoomAndPan('west'); return false;">&larr;</a></h2></td>
					<td></td>
					<td><h2 id="panRight"><a href="javascript:void(0);"
						onmousedown="ZoomAndPan('east'); return false;">&rarr;</a></h2></td>
				</tr>
				<tr>
					<td></td>
					<td><h2 id="panDown"><a href="javascript:void(0);"
						onmousedown="ZoomAndPan('south'); return false;">&darr;</a></h2></td>
					<td></td>
				</tr>
			</table>

			<h2 style="text-align:center">
				<a id="zoomIn" href="javascript:void(0);"
					onmousedown="ZoomAndPan('in'); return false;">+</a>&nbsp;&nbsp;
				<a id="zoomOut" href="javascript:void(0);"
				onmousedown="ZoomAndPan('out'); return false;">-</a>
			</h2>

			<table cellspacing="0" cellpadding="0" border="0"
				style="clear:left; width:100%; text-align:center">
				<tr>
					<td style="text-align:left; width:1%">
						<h5>Selection <a href="#" onclick="CenterSelection(); return false;">(CENTER)</a></h5></td>
					<td style="text-align:right">
						<h5><span id="printLatS">37.500</span>&deg;N - <span id="printLatN">39.500</span>&deg;N</h5>
						<h5><span id="printLongW">-118.500</span>&deg;W - <span id="printLongE">-115.500</span>&deg;W</h5>
					</td>
				</tr>
				<tr>
					<td style="text-align:left"><h5>View <a href="#" onclick="ResetOrientation('<?= String::websafe(Preference::getInstance()->getInitialViewViewBox()) ?>'); CenterSelection(); return false;">(RESET)</a></h5></td>
					<td style="text-align:right">
						<h5><span id="covLatN">34.900</span>&deg;N - <span id="covLatS">42.100</span>&deg;N</h5>
						<h5><span id="covLongW">-120.600</span>&deg;W - <span id="covLongE">-113.400</span>&deg;W</h5>
					</td>
				</tr>
			</table>
		</div>

		<? include_once('./templates/includes/menu.html.php'); ?>

	</div> <!-- #menu -->

	<div id="mainDiv">

		<h2 class="title">ISIS <span style="font-weight:normal; font-size:0.7em">(Interactive Spatial Image Search)</span> @ UNLV</h2>
		<!--[if IE]>
		<p>If you don't see the map, you must download the <a href="http://www.adobe.com/svg/viewer/install/">Adobe SVG Viewer</a> plugin.</p>
		<![endif]-->

		<div class="mainSubDiv">
			<? if (View::isFlash()): ?>
				<?= String::websafe(View::getFlash()) ?>
			<? endif ?>

			<embed id="svgMap" src="mainview.svg.php" type="image/svg+xml"
				pluginspage="http://www.adobe.com/svg/viewer/install/">
			</embed>

			<div class="navSubDiv">
				<form method="get" action="results.php">
					<fieldset>
						<legend class="navHeader">Search For...</legend>

						<ul style="float:left">
							<li>
								<label>
									<input type="radio" name="search" value="w"
										checked="checked" />
									<img src="templates/images/within.png" alt="Entirely Within"
										style="vertical-align:middle; margin:0 0 5px 0" />
									Maps Entirely Within
								</label>
							</li>
							<li>
								<label>
									<input type="radio" name="search" value="p" />
									<img src="templates/images/partially_within.png" alt="Partially Within"
										style="vertical-align:middle; margin:0 0 5px 0" />
									Maps At Least Within
								</label>
							</li>
							<li>
								<label>
									<input type="radio" name="search" value="o" />
									<img src="templates/images/outside.png" alt="Outside"
										style="vertical-align:middle; margin:0 0 5px 0" />
									Maps Outside
								</label>
							</li>
						</ul>
						<input type="submit" class="mapSearch" value="Search"
							style="float:right" />
					</fieldset>

					<fieldset style="margin-bottom:0px">
						<legend class="navHeader">Search In...</legend>

						<? if (DataStore::isAvailable()): ?>
							<ul>
							<? foreach (Collection::getAll() as $c): ?>
								<li>
									<label>
										<input type="checkbox"
											name="collection_ids[]"
											value="<?= String::websafe($c->getID()) ?>"
											checked="checked" />
										<? if (strlen($c->getIntroURL()) > 0): ?>
											<a href="<?= String::websafe($c->getIntroURL()) ?>">
										<? endif ?>
										<?= String::websafe($c->getName()) ?>
										<? if (strlen($c->getIntroURL()) > 0): ?>
											</a>
										<? endif ?>
										(<?= String::websafe($c->getOrganization()) ?>)
									</label>
								</li>
							<? endforeach ?>
							</ul>
						<? endif ?>

						<input type="hidden" id="long_w" name="lw"
							value="<?= String::websafe(Preference::getInstance()->getDefaultFrameLongW()) ?>"/>
						<input type="hidden" id="lat_s" name="ls"
							value="<?= String::websafe(Preference::getInstance()->getDefaultFrameLatS()) ?>"/>
						<input type="hidden" id="long_e" name="le"
							value="<?= String::websafe(Preference::getInstance()->getDefaultFrameLongE()) ?>"/>
						<input type="hidden" id="lat_n" name="ln"
							value="<?= String::websafe(Preference::getInstance()->getDefaultFrameLatN()) ?>"/>
						<? // values below are filled in by mouseMove() in pan.js ?>
						<input type="hidden" id="vb" name="vb"
							value="<?= String::websafe(Preference::getInstance()->getDefaultViewViewBox()) ?>"/>
						<input type="hidden" id="view" name="view" value="grid"/>
						<input type="hidden" id="page" name="page" value="1"/>
						<input type="hidden" id="group" name="group" value="true"/>
						<input type="hidden" id="sort" name="sort" value="area"/>
						<input type="hidden" id="order" name="order" value="asc"/>
					</fieldset>
				</form>
			</div> <!-- navSubDiv -->

		</div> <!-- mainSubDiv -->

	</div> <!-- mainDiv -->

	<div class="clear">&nbsp;</div>

</div> <!-- container -->

<? @include('./templates/includes/footer.html'); ?>

</body>

</html>
