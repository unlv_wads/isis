<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title>ISIS @ UNLV: Error</title>

	<? if (is_readable($_SERVER['DOCUMENT_ROOT'] . '/dc_styles/default.css')): ?>
		<link rel="stylesheet" type="text/css" media="screen"
			href="/dc_styles/default.css"/>
	<? else: ?>
		<link rel="stylesheet" type="text/css" media="screen"
			href="templates/css/unlv.css"/>
	<? endif ?>
	<link rel="stylesheet" type="text/css" media="screen"
		href="templates/css/main.css"/>

</head>

<body>

<? include_once('templates/includes/header.html.php'); ?>

<div id="container">


	<div id="mainDiv">

		<h2 class="title">ISIS Error</h2>

		<? foreach (CustomException::getAll() as $e): ?>
			<p><?= String::websafe($e->getMessage()) ?></p>

			<? if (!$e instanceof PreferencesException): ?>
				<? if (Preference::getInstance()->isDebugMode()): ?>
					<code><pre><?= String::websafe(wordwrap($e->getTraceAsString(), 80)) ?></pre></code>
				<? endif ?>
			<? endif ?>
		<? endforeach ?>

		<? if (!$e instanceof PreferencesException): ?>
			<h5>Version <?= Preference::getInstance()->getVersion() ?>
			<? if (Preference::getInstance()->isDebugMode()): ?>
				&nbsp;&nbsp;<em style="color:red">(DEBUG MODE ON)</em>
			<? endif ?>
			</h5>
		<? endif ?>

	</div> <!-- mainDiv -->


</div> <!-- container -->

<? @include('templates/includes/footer.html'); ?>

</body>

</html>
