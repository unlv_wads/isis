<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

<svg id="panSVG" width="100%" height="100%" version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink"
	preserveAspectRatio="xMidYMid meet"
	viewBox="<?= $ctrl->getView()->getCanvas()->getViewBox() ?>"
	onload="init()"
	onmouseup="mouseUp(evt)"
	onmousemove="mouseMove(evt)">

<script type="text/ecmascript" xlink:href="./js/functions.js.php" />
<script type="text/ecmascript" xlink:href="./js/pan.js.php" />


<rect id="background" x="-140" y="-50" height="30" width="50"/>

<!-- other geodata -->
<?php include('./geodata/county_borders.svg');

echo sprintf('<rect id="pannedArea" x="%f" y="%f"
	dragx="%f" dragy="%f" width="%f" height="%f" />',
	Preference::getInstance()->getInitialViewX(),
	Preference::getInstance()->getInitialViewY(),
	Preference::getInstance()->getInitialViewX(),
	Preference::getInstance()->getInitialViewY(),
	Preference::getInstance()->getInitialViewWidth(),
	Preference::getInstance()->getInitialViewHeight()
);

?>

</svg>
