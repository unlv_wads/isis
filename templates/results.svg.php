<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

<svg id="svgResults" width="100%" height="100%" version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink"
	preserveAspectRatio="xMidYMid meet"
	viewBox="<?= $ctrl->getView()->getCanvas()->getViewBox() ?>">

	<rect id="background" x="-140" y="-50" height="30" width="50"/>

	<!-- insert geodata here -->
	<? include('./geodata/state_borders.svg'); ?>

	<?= $ctrl->getView()->getFormattedResults($ctrl->getSearch()); ?>

</svg>
