<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg id="rootSVG" width="100%" height="100%" version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink"
	preserveAspectRatio="xMidYMid meet"
	viewBox="<?= $ctrl->getView()->getCanvas()->getViewBox(); ?>"
	onload="init()"
	onmouseup="mouseUp(evt)"
	onmousemove="mouseMove(evt)">

<script type="text/ecmascript" xlink:href="./js/functions.js.php" />
<script type="text/ecmascript" xlink:href="./js/selection.js.php" />

<defs>

	<!-- This is an example of using raster geodata for the basemap. This
	is only a *definition* of the raster file; to make the file actually
	show up, you must <use> it outside of this <defs> section. An example
	appears further below, also commented out. -->
	<!--
	<image id="nevada_raster" x="0" y="0"
		width="1062" height="1067" xlink:href="./geodata/bg.jpg"/>
	-->

	<circle id="cityPoint" class="cityPoint" cx="0" cy="0" r="0.08"/>

</defs>

<rect id="background" x="-140" y="-50" height="30" width="40"/>

<!-- This is an example of using raster geodata for the basemap. In the
<defs> section, we defined the nevada_raster image. Here, we display it.
You will have to tweak the translate() and scape() parameters to align
it properly within the viewport. The values in translate() are the
coordinates of the upper left corner of the data, and the value in scale() is
a multiplier from 0 to 1 (100%). Remember that pixels correspond 1:1 to map
coordinates, so if you are using decimal degrees, a 1000x1000 pixel image at
a scale of 1 will be many earths large. (Hence the need for downscaling.) -->
<!--
<use id="bg_topo" x="0" y="0"
	transform="translate(-120.88, -42.46), scale(0.00735)"
	xlink:href="#nevada_raster" style="pointer-events:none" />
-->

<!-- lat/long lines -->
<g id="lines" display="none">
	<?= $ctrl->getView()->getCanvas()->getGraticule(); ?>
</g>

<?php

/* This is an example of using vector data in the map - either as the basemap
or as layers above it. Replace these filenames with your own, or else comment
out the corresponding include() line. */
include('./geodata/county_borders.svg');
include('./geodata/county_labels.svg');
include('./geodata/state_borders.svg');
include('./geodata/cities.svg');

echo sprintf('<rect id="frame" x="%f" y="%f" width="%f" height="%f"
	dragx="%f" dragy="%f" dragw="%f" dragh="%f" />',
	$ctrl->getView()->getFrame()->getX(),
	$ctrl->getView()->getFrame()->getY(),
	$ctrl->getView()->getFrame()->getWidth(),
	$ctrl->getView()->getFrame()->getHeight(),
	$ctrl->getView()->getFrame()->getX(),
	$ctrl->getView()->getFrame()->getY(),
	$ctrl->getView()->getFrame()->getWidth(),
	$ctrl->getView()->getFrame()->getHeight()
);

echo sprintf('<rect id="resizer" x="%f" y="%f" width="%f" height="%f"
	dragx="%f" dragy="%f" />',
	$ctrl->getView()->getResizer()->getX(),
	$ctrl->getView()->getResizer()->getY(),
	$ctrl->getView()->getResizer()->getWidth(),
	$ctrl->getView()->getResizer()->getHeight(),
	$ctrl->getView()->getResizer()->getX(),
	$ctrl->getView()->getResizer()->getY()
);

?>

</svg>
