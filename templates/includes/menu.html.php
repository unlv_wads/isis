<ul>

<li><a href="/digital_collections.php">List of Digital Collections</a></li>
<li><a href="/">Digital Collections Home</a></li>

            <li>View Items by Format
                <form method="get" action="/cdm4/results.php">
                    <input type="hidden" name="CISOOP1" value="any" />
                    <input type="hidden" name="CISORESTMP" value="results.php" />
                    <input type="hidden" name="CISOVIEWTMP" value="item_viewer.php" />
                    <input type="hidden" name="CISOMODE" value="grid" />
                    <input type="hidden" name="CISOGRID" value="thumbnail,A,1;title,A,1;subjec,A,0;descri,200,0;none,A,0;20;title,none,none,none,none" />
                    <input type="hidden" name="CISOBIB" value="title,A,1,N;subjec,A,0,N;descri,200,0,N;none,A,0,N;none,A,0,N;20;title,none,none,none,none" />
                    <input type="hidden" name="CISOTHUMB" value="20 (4x5);title,none,none,none,none" />
                    <input type="hidden" name="CISOTITLE" value="20;title,none,none,none,none" />
                    <input type="hidden" name="CISOHIERA" value="20;subjec,title,none,none,none" />
                    <input type="hidden" name="CISOSUPPRESS" value="0" />
                    <select name="CISOPARM" class="format">
                        <option value="all:relatik:maps">Maps</option>
                        <option value="all:type:Image">Images</option>
                        <option value="all:type:text">Text and Documents</option>
                        <option value="all:title:oral history">Oral Histories</option>
                        <option value="all:CISOSEARCHALL:audio, video, MP3">Audio and Video</option>
                    </select>              
                    <input type="image" src="templates/images/button_go.gif" class="gobutton" />
            	</form>
            </li>
            <li>Search Across All Digital Collections
                <form method="get" action="/cdm4/results.php">
                    <!-- contentdm CQ search code -->
                    <input type="hidden" name="CISOOP1" value="all" />
                    <input type="hidden" name="CISOFIELD1" value="CISOSEARCHALL" />
                    <input type="hidden" name="CISORESTMP" value="/cdm4/results.php" />
                    <input type="hidden" name="CISOVIEWTMP" value="/cdm4/item_viewer.php" />
                    <input type="hidden" name="CISOMODE" value="grid" />
                    <input type="hidden" name="CISOGRID" value="thumbnail,A,1;title,A,1;subjec,A,0;descri,200,0;none,A,0;20;title,none,none,none,none" />
                    <input type="hidden" name="CISOBIB" value="title,A,1,N;subjec,A,0,N;descri,200,0,N;none,A,0,N;none,A,0,N;20;title,none,none,none,none" />
                    <input type="hidden" name="CISOTHUMB" value="20 (4x5);title,none,none,none,none" />
                    <input type="hidden" name="CISOTITLE" value="20;title,none,none,none,none" />
                    <input type="hidden" name="CISOHIERA" value="20;subjec,title,none,none,none" />
                    <input name="CISOBOX1" class="search" />
                    <input type="hidden" name="CISOROOT" value="all" />
                    <input type="image" src="templates/images/button_go.gif" class="gobutton" />
                </form>
                <span class="example">(e.g., Las Vegas)</span>
            </li>
           	<? if ($secondary == "yes") {
					echo "	<li>Search UNLV &amp; UNR Collections
								<!-- Google CSE Search Box Begins  -->
								<form action=\"http://contentdm.library.unr.edu/nvdigitalcse.html\" id=\"searchbox_006820933171735136265:cn2sptebdvu\">
									<input type=\"hidden\" name=\"cx\" value=\"006820933171735136265:cn2sptebdvu\" />
									<input type=\"hidden\" name=\"cof\" value=\"FORID:9\" />
									<input type=\"text\" name=\"q\" class=\"search\" />            
									<input type=\"image\" src=\"images/button_go.gif\" class=\"gobutton\" />
								</form>
								<script type=\"text/javascript\" src=\"http://www.google.com/coop/cse/brand?form=searchbox_006820933171735136265%3Acn2sptebdvu\"></script>
								<!-- Google CSE Search Box Ends -->
							</li>";
					} 
			?>
	            	
            <li><a href="/cdm4/search.php">Advanced Search</a></li>
            <?
				if ($secondary == "yes") {
					echo "<li><a href=\"/cdm4/preferences.php\">Preferences</a></li>";
					echo "<li><a href=\"/cdm4/favorites.php\">My Favorites</a></li>";
					echo "<li><a href=\"/cdm4/help.php\">Help</a></li>";
					}
			?>
        </ul>
        