<div id="header">
    <img src="templates/images/top_bar.gif" alt="top bar" width="756" height="80" usemap="#m_top_bar" />
    <map name="m_top_bar" id="m_top_bar">
        <area shape="rect" coords="0,17,258,61" href="http://www.library.unlv.edu/" title="Libraries home page" alt="Libraries home page" />
        <area shape="rect" coords="666,5,751,31" href="http://www.unlv.edu/" title="unlv" alt="unlv" />
        <area shape="rect" coords="663,34,756,55" href="http://www.library.unlv.edu/contact.html" title="Contact Us" alt="Contact Us" />
        <area shape="rect" coords="563,35,659,56" href="http://www.library.unlv.edu/sitemap.html" title="site map" alt="site map" />
        <area shape="rect" coords="463,36,557,57" href="https://webpac.library.unlv.edu/patroninfo~S10/" title="Your Account" alt="Your Account" />
        <area shape="rect" coords="382,34,457,55" href="http://webpac.library.unlv.edu/search~S1/" title="catalog" alt="catalog" />
        <area shape="rect" coords="297,35,377,56" href="http://www.library.unlv.edu/about/hours.html" title="hours" alt="hours" />
    </map>
</div> <!-- end header -->
