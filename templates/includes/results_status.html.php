<table border="0" cellpadding="0" cellspacing="0" class="resultsStatusDiv">
	<tr>
		<td>
			<h3>Sort: <?= $ctrl->getView()->getSortLinks($ctrl->getSearch()) ?></h3>
		</td>

		<td style="text-align:right">
			<h3><?= $ctrl->getView()->getViewLinks() ?></h3>
		</td>
	</tr>

	<tr>
		<td>
			<h3>Grouping: <?= $ctrl->getView()->getGroupLinks() ?></h3>
		</td>

		<td style="text-align:right">
			<h3>Page: <?= $ctrl->getView()->getPageLinks($ctrl->getSearch()); ?></h3>
		</td>
	</tr>
</table>
