<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title>ISIS @ UNLV: Search Results</title>

	<script type="text/javascript" src="./js/functions.js.php"></script>
	<script type="text/javascript" src="./templates/js/autoscroll.js"></script>

	<? if (is_readable($_SERVER['DOCUMENT_ROOT'] . '/dc_styles/default.css')): ?>
		<link rel="stylesheet" type="text/css" media="screen"
			href="/dc_styles/default.css"/>
	<? else: ?>
		<link rel="stylesheet" type="text/css" media="screen"
			href="./templates/css/unlv.css"/>
	<? endif ?>
	<link rel="stylesheet" type="text/css" media="screen"
		href="./templates/css/main.css"/>

</head>

<body onload="initEmbed('results');">

<? include_once('./templates/includes/header.html.php'); ?>

<div id="container">

	<div id="menu">
		<h1>Digital Collections</h1>
	</div>

	<div id="mainDiv">
		<? include('./templates/includes/results_status.html.php');

		echo $ctrl->getView()->getFormattedResults($ctrl->getSearch());

		include('./templates/includes/results_status.html.php'); ?>
	</div> <!-- mainDiv -->

	<div id="resultsMapDiv" <? if ($ctrl->getSearch()->getNumResults() > 0) echo 'style="width:256px"'; ?>>
		<? if ($ctrl->getSearch()->getNumResults() > 0): ?>
			<div class="navSubDiv">

				<h4 class="navHeader"><?= String::websafe($ctrl->getSearch()->getNumResults()) ?> map<?
					if ($ctrl->getSearch()->getNumResults() <> 1) echo "s";
					switch ($_GET['search']) {
						case 'w': $where = 'entirely within'; break;
						case 'p': $where = 'partially within'; break;
						case 'o': $where = 'completely outside'; break;
					} ?>
					<span style="font-style:italic"><?= $where ?></span> selection
					<span style="font-size:0.8em">
						<a href="index.php">(search again?)</a>
					</span>
				</h4>

				<? $q = $_GET; unset($q['sort'], $q['order'], $q['page'], $q['view']); ?>

				<embed id="svgResults" src="results.svg.php?<?= http_build_query($q) ?>"
					type="image/svg+xml" width="200" height="200"></embed>

				<? if ($_GET['search'] == 'w'): ?>
					<h5 style="text-align:center">
						<a id="zoom" href="#" onclick="ResultsMapZoom('<?= String::websafe(Preference::getInstance()->getDefaultMapViewBox()) ?>');
							return false; ">- Zoom Out</a></h5>
				<? endif ?>
			</div>
		<? endif ?>

	</div> <!-- resultsMapDiv -->

	<div class="clear">&nbsp;</div>

</div> <!-- #container -->

<? include_once('./templates/includes/footer.html'); ?>

</body>
</html>
