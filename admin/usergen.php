<?php
/*****************************************************************************
Copyright © 2008 The Regents of the University of Nevada
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

if (array_key_exists('username', $_POST)
&& array_key_exists('password', $_POST)) {
	$username = $_POST['username'];
	$password = sha1($_POST['password']);
}

?>

<html>
<head>
	<title>ISIS User Generator</title>
	<style type="text/css">
		ul {
			list-style-type:none;
			margin-left:0px;
			padding-left:0px;
		}
	</style>
</head>
<body>

<h2>ISIS User Generator</h2>

<? if ($username && $password): ?>

	<p>User generated. Paste the following code into the <code>&lt;users&gt;</code> block of your config.xml.php file:</p>

	<code><pre>
&lt;user&gt;
   &lt;username&gt;<?= htmlspecialchars($username) ?>&lt;/username&gt;
   &lt;password_hash&gt;<?= htmlspecialchars($password) ?>&lt;/password_hash&gt;
&lt;/user&gt;
	</pre></code>

<? else: ?>

	<p>ISIS users are stored in the config.xml.php file with a SHA-1 password hash. Use this script to generate new users.</p>


	<form action="" method="post">
		<fieldset>
			<ul>
				<li>
					<label for="username">Username</label>
					<input type="text" name="username" id="username"
						size="16" />
				<li>
					<label for="password">Password
					<input type="password" name="password" id="password"
						size="16" />
				</li>
			</ul>
			<input type="submit" value="Generate User" />
		</fieldset>
	</form>

<? endif ?>

</body>
</html>