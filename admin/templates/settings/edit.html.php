<? include_once('../templates/includes/head.html.php') ?>

<body onload="toggleDBFields('storage_engine')">

<script type="text/javascript">
function toggleDBFields(select_id) {
	var elements = new Array('dbengine', 'dbhost', 'dbport', 'dbname',
		'dbuser', 'dbpass');
	var attr = true;
	if (document.getElementById(select_id).value == 'rdbms') attr = false;
	for (i = 0; i < elements.length; i++) {
		document.getElementById(elements[i]).disabled = attr;
	}
}
</script>

<div id="wrap">

	<? include_once('../templates/includes/header.html.php') ?>

	<? include_once('../templates/includes/menu.html.php') ?>

	<h5 class="breadcrumb">
		<a href="../">Home</a>
		&rArr; Configuration
	</h5>

	<div id="content">

		<? if (View::isFlash()): ?>
			<div class="flash">
				<?= View::getFlash() ?>
			</div>
		<? endif ?>

		<form method="post" action="">
			<fieldset>
				<legend>ISIS Configuration</legend>

				<ul class="form">
					<li>
						<label><input id="debug_mode" name="debug_mode" value="true"
							type="checkbox"
							<? if (Preference::getInstance()->isDebugMode()): ?>
							checked="checked" <? endif ?> /> Debug mode</label>
						(displays debugging information on error pages and loosens the error reporting threshold)
					</li>
				</ul>
			</fieldset>

			<fieldset>
				<legend>Data store</legend>
				<ul class="form">
					<li>
						<label for="storage_engine">Storage Engine:</label>
						<select id="storage_engine" name="storage_engine"
						onchange="toggleDBFields(this.id)">
							<option value="rdbms"
								<? if (Preference::getInstance()->getStorageEngine() == 'rdbms'): ?>
								selected="selected" <? endif ?>>Relational Database</option>
							<option value="xmlfile"
								<? if (Preference::getInstance()->getStorageEngine() == 'xmlfile'): ?>
								selected="selected" <? endif ?>>XML File</option>
						</select>
					</li>
					<li>
						<label for="dbengine">DBMS:</label>
						<select id="dbengine" name="dbengine">
							<option value="mysql"
								<? if (Preference::getInstance()->getDBEngine() == 'mysql'): ?>
								selected="selected" <? endif ?>>MySQL</option>
							<option value="pgsql"
								<? if (Preference::getInstance()->getDBEngine() == 'pgsql'): ?>
								selected="selected" <? endif ?>>PostgreSQL</option>
						</select>
					</li>
					<li>
						<label for="dbhost">Host:</label>
						<input id="dbhost" name="dbhost" type="text"
							value="<?= String::websafe(Preference::getInstance()->getDBHost()) ?>" />
					</li>
					<li>
						<label for="dbport">Port:</label>
						<input id="dbport" name="dbport" type="text"
							value="<?= String::websafe(Preference::getInstance()->getDBPort()) ?>" />
					</li>
					<li>
						<label for="dbname">Name:</label>
						<input id="dbname" name="dbname" type="text"
							value="<?= String::websafe(Preference::getInstance()->getDBName()) ?>" />
					</li>
					<li>
						<label for="dbuser">Username:</label>
						<input id="dbuser" name="dbuser" type="text"
							value="<?= String::websafe(Preference::getInstance()->getDBUser()) ?>" />
					</li>
					<li>
						<label for="dbpass">Password:</label>
						<input id="dbpass" name="dbpass" type="password"
							value="<?= String::websafe(Preference::getInstance()->getDBPass()) ?>" />
					</li>
				</ul>
			</fieldset>
			<fieldset>
				<legend>SVG Maps</legend>
				<ul class="form">
					<li>
						<label>Coordinate digit precision:
						<select name="num_coordinate_digits">
							<? foreach (range(0, 9) as $d): ?>
								<option value="<?= $d ?>"
									<? if (Preference::getInstance()->getNumMapCoordinateDigits() == $d): ?>
									selected="selected"<?endif?>>
									<?= $d ?>
								</option>
							<? endforeach ?>
						</select> decimal places</label>
					</li>
					<li>
						<label><input name="enforce_bounds" value="true"
							type="checkbox"
							<? if (Preference::getInstance()->shouldEnforcePanMapBounds()): ?>
							checked="checked" <? endif ?> /> Enforce pan map bounds</label>
					</li>
					<li>
						<label for="pan_factor">Pan factor:</label>
						<input id="pan_factor" name="pan_factor" type="text"
							size="5" maxlength="5"
							value="<?= String::websafe(Preference::getInstance()->getPanFactor()) ?>" />
							(default is 0.25)
					</li>
					<li>
						<label for="scale_factor">Scale factor:</label>
						<input id="scale_factor" name="scale_factor" type="text"
							size="5" maxlength="5"
							value="<?= String::websafe(Preference::getInstance()->getScaleFactor()) ?>" />
							(default is 0.2)
					</li>
					<li>
						<label>Canvas bounds:</label>
						<table cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td></td>
								<td>
									<label>
										<input name="canvas_n" type="text"
											size="8" maxlength="10"
											value="<?= String::websafe(Preference::getInstance()->getMapLatN()) ?>"
										/>&deg;N
									</label>
								</td>
								<td></td>
							</tr>
							<tr>
								<td>
									<label>
										<input name="canvas_w" type="text"
											size="8" maxlength="10"
											value="<?= String::websafe(Preference::getInstance()->getMapLongW()) ?>"
										/>&deg;W
									</label>
								</td>
								<td></td>
								<td>
									<label>
										<input name="canvas_e" type="text"
											size="8" maxlength="10"
											value="<?= String::websafe(Preference::getInstance()->getMapLongE()) ?>"
										/>&deg;E
									</label>
								</td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<label>
										<input name="canvas_s" type="text"
											size="8" maxlength="10"
											value="<?= String::websafe(Preference::getInstance()->getMapLatS()) ?>"
										/>&deg;S
									</label>
								</td>
								<td></td>
							</tr>
						</table>
					</li>
					<li>
						<label>Initial view bounds:</label>
						<table cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td></td>
								<td>
									<label>
										<input name="iv_n" type="text"
											size="8" maxlength="10"
											value="<?= String::websafe(Preference::getInstance()->getInitialViewLatN()) ?>"
										/>&deg;N
									</label>
								</td>
								<td></td>
							</tr>
							<tr>
								<td>
									<label>
										<input name="iv_w" type="text"
											size="8" maxlength="10"
											value="<?= String::websafe(Preference::getInstance()->getInitialViewLongW()) ?>"
										/>&deg;W
									</label>
								</td>
								<td></td>
								<td>
									<label>
										<input name="iv_e" type="text"
											size="8" maxlength="10"
											value="<?= String::websafe(Preference::getInstance()->getInitialViewLongE()) ?>"
										/>&deg;E
									</label>
								</td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<label>
										<input name="iv_s" type="text"
											size="8" maxlength="10"
											value="<?= String::websafe(Preference::getInstance()->getInitialViewLatS()) ?>"
										/>&deg;S
									</label>
								</td>
								<td></td>
							</tr>
						</table>
					</li>
					<li>
						<label for="min_coverage_ns">Minimum coverage N-S:</label>
						<input id="min_coverage_ns" name="min_coverage_ns" type="text"
							size="5" maxlength="5"
							value="<?= String::websafe(Preference::getInstance()->getMapMinCoverageNS()) ?>" />
					</li>
					<li>
						<label for="min_coverage_ew">Minimum coverage E-W:</label>
						<input id="min_coverage_ew" name="min_coverage_ew" type="text"
							size="5" maxlength="5"
							value="<?= String::websafe(Preference::getInstance()->getMapMinCoverageEW()) ?>" />
					</li>
					<li>
						<label>Default selection frame bounds:</label>
						<table cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td></td>
								<td>
									<label>
										<input name="frame_n" type="text"
											size="8" maxlength="10"
											value="<?= String::websafe(Preference::getInstance()->getDefaultFrameLatN()) ?>"
										/>&deg;N
									</label>
								</td>
								<td></td>
							</tr>
							<tr>
								<td>
									<label>
										<input name="frame_w" type="text"
											size="8" maxlength="10"
											value="<?= String::websafe(Preference::getInstance()->getDefaultFrameLongW()) ?>"
										/>&deg;W
									</label>
								</td>
								<td></td>
								<td>
									<label>
										<input name="frame_e" type="text"
											size="8" maxlength="10"
											value="<?= String::websafe(Preference::getInstance()->getDefaultFrameLongE()) ?>"
										/>&deg;E
									</label>
								</td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<label>
										<input name="frame_s" type="text"
											size="8" maxlength="10"
											value="<?= String::websafe(Preference::getInstance()->getDefaultFrameLatS()) ?>"
										/>&deg;S
									</label>
								</td>
								<td></td>
							</tr>
						</table>
					</li>
					<li>
						<label for="resizer_thickness">Resizer thickness:</label>
						<input id="resizer_thickness" name="resizer_thickness"
							type="text" size="5" maxlength="5"
							value="<?= String::websafe(Preference::getInstance()->getResizerThickness()) ?>" />
					</li>
				</ul>
			</fieldset>

			<fieldset>
				<legend>Results</legend>
				<ul class="form">
					<li>
						<label for="grid_maxwords">Max # of words to display in results (grid view):</label>
						<input id="grid_maxwords" name="grid_maxwords"
							type="text" size="3" maxlength="3"
							value="<?= String::websafe(Preference::getInstance()->getGridMaxWords()) ?>" />
					</li>
					<li>
						<label for="tile_maxwords">Max # of words to display in results (tile view):</label>
						<input id="tile_maxwords" name="tile_maxwords"
							type="text" size="3" maxlength="3"
							value="<?= String::websafe(Preference::getInstance()->getTileMaxWords()) ?>" />
					</li>
					<li>
						<label for="map_outlines_limit">Max # of map outlines to display in results map:</label>
						<input id="map_outlines_limit" name="map_outlines_limit"
							type="text" size="5" maxlength="5"
							value="<?= String::websafe(Preference::getInstance()->getResultsMapOutlinesLimit()) ?>" />
					</li>
					<li>
						<label for="grid_rows">Rows (grid view):</label>
						<input id="grid_rows" name="grid_rows"
							type="text" size="2" maxlength="2"
							value="<?= String::websafe(Preference::getInstance()->getGridRows()) ?>" />
					</li>
					<li>
						<label for="tile_rows">Rows (tile view):</label>
						<input id="tile_rows" name="tile_rows"
							type="text" size="2" maxlength="2"
							value="<?= String::websafe(Preference::getInstance()->getTileRows()) ?>" />
					</li>
					<li>
						<label for="tile_cols">Columns (tile view):</label>
						<input id="tile_cols" name="tile_cols"
							type="text" size="3" maxlength="3"
							value="<?= String::websafe(Preference::getInstance()->getTileColumns()) ?>" />
					</li>
					<li>
						<table class="dataTable" cellspacing="0" cellpadding="0" border="0">
							<caption>Fields to display in grid view:</caption>
							<tr>
								<th>Field</th>
								<th>Name</th>
								<th>Display?</th>
							</tr>
							<? foreach (Preference::getInstance()->getAllFields() as $f): ?>
							<tr>
								<td><?= String::websafe($f->getMapping()) ?></td>
								<td>
									<? if (!in_array($f->getMapping(), array('index', 'thumb'))): ?>
										<input type="text"
										name="field_name[<?= String::websafe($f->getMapping()) ?>]"
										value="<?= String::websafe($f->getName()) ?>" />
									<? endif ?>
								</td>
								<td>
									<input type="checkbox"
									name="field_display[<?= String::websafe($f->getMapping()) ?>]"
									value="true"
									<? if (in_array($f, Preference::getInstance()->getDisplayFields())): ?>
										checked="checked"
									<? endif ?> />
									<input type="hidden" name="all_field_mappings[]"
									value="<?= String::websafe($f->getMapping()) ?>" />
								</td>
							</tr>
							<? endforeach ?>
						</table>
					</li>
				</ul>
			</fieldset>

			<input type="submit" name="edit" value="Save Changes" />

		</form>

	</div> <!-- #content -->

	<? include_once('../templates/includes/footer.html.php') ?>

</div> <!-- #wrap -->

</body>
</html>
