<div id="menu">
	<ul>
		<li><a href="/<?= trim(Config::ISIS_FOLDER_ROOT, '/') ?>/admin/">Home</a></li>
		<li><a href="/<?= trim(Config::ISIS_FOLDER_ROOT, '/') ?>/admin/settings/">Configuration</a></li>
		<li><a href="/<?= trim(Config::ISIS_FOLDER_ROOT, '/') ?>/admin/maps/">Maps</a></li>
		<li><a href="/<?= trim(Config::ISIS_FOLDER_ROOT, '/') ?>/admin/collections/">Collections</a></li>
		<li><a href="/<?= trim(Config::ISIS_FOLDER_ROOT, '/') ?>/admin/export.php">Export</a></li>
		<li><a href="/<?= trim(Config::ISIS_FOLDER_ROOT, '/') ?>/admin/login.php?action=logout">Logout</a></li>
		<li style="color:#606060"><?= (User::isLoggedIn()) ? String::websafe(User::getInstance()->getUsername()) : '' ?></li>
	</ul>
</div> <!-- #menu -->
