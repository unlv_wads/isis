<ul class="form">
	<li>
		<label for="name">Name:</label>
		<input id="name" name="name" class="medium"
			value="<?= String::websafe($c->getName()) ?>" /> <em>(Another Great Digital Collection)</em>
	</li>
	<li>
		<label for="alias">Alias (&quot;CISOROOT&quot;):</label>
		<input id="alias" name="alias" class="medium"
			value="<?= String::websafe($c->getAlias()) ?>" /> <em>(/stuff)</em>
	</li>
	<li>
		<label for="intro_url">Intro URL:</label>
		<input id="intro_url" name="intro_url" class="medium"
			value="<?= String::websafe($c->getIntroURL()) ?>" /> <em>(http://www.site.edu/thiscollection/)</em>
	</li>
	<li>
		<label for="organization">Organization:</label>
		<input id="organization" name="organization" class="medium"
			value="<?= String::websafe($c->getOrganization()) ?>" /> <em>(Another Great Digital Collections Unit)</em>
	</li>
	<li>
		<label for="org_url">Org. URL:</label>
		<input id="org_url" name="org_url" class="medium"
			value="<?= String::websafe($c->getOrganizationURL()) ?>" /> <em>(http://www.site.edu/)</em>
	</li>
	<li>
		<label for="org_url">Base URL:</label>
		<input id="base_url" name="base_url" class="medium"
			value="<?= String::websafe($c->getBaseURL()) ?>" /> <em>(http://your.cdmserver.edu/)</em>
	</li>
	<li>
		<label for="path_to_oai">Path to oai.exe:</label>
		<input id="path_to_oai" name="path_to_oai" class="short"
			value="<?= String::websafe($c->getPathToOAI()) ?>" /> <em>(/cgi-bin/oai.exe)</em>
	</li>
	<li>
		<label for="path_to_thumbnail">Path to thumbnail.exe:</label>
		<input id="path_to_thumbnail" name="path_to_thumbnail"
			class="short"
			value="<?= String::websafe($c->getPathToThumbnail()) ?>" /> <em>/cgi-bin/thumbnail.exe)</em>
	</li>
	<li>
		<label for="oai_id_prefix">OAI ID Prefix:</label>
		<input id="oai_id_prefix" name="oai_id_prefix" class="short"
			value="<?= String::websafe($c->getOAIIDPrefix()) ?>" /> <em>(oai:your.cdmserver.edu:)</em>
	</li>
</ul>
