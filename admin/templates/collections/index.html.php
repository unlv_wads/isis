<? include_once('../templates/includes/head.html.php') ?>

<body>

<div id="wrap">

	<? include_once('../templates/includes/header.html.php') ?>

	<? include_once('../templates/includes/menu.html.php') ?>

	<h5 class="breadcrumb">
		<a href="../">Home</a>
		&rArr; Collections
	</h5>

	<div id="content">

		<? if (View::isFlash()): ?>
			<div class="flash">
				<?= View::getFlash() ?>
			</div>
		<? endif ?>

		<h5><a href="add.php">Add</a></h5>

		<form method="post" action="">
			<table class="dataTable" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<th colspan="2"></th>
					<th>Name</th>
					<th>Organization</th>
				</tr>
				<? foreach (Collection::getAll() as $c): ?>
					<tr>
						<td><input type="checkbox" name="collections[]"
							value="<?= String::websafe($c->getID()) ?>" /></td>
						<td><a href="edit.php?id=<?= String::websafe($c->getID()) ?>">Edit</a></td>
						<td><a href="<?= String::websafe($c->getIntroURL()) ?>">
							<?= String::websafe($c->getName()) ?></a></td>
						<td>
							<a href="<?= String::websafe($c->getOrganizationURL()) ?>">
								<?= String::websafe($c->getOrganization()) ?>
							</a>
						</td>
					</tr>
				<? endforeach ?>
			</table>

			<!-- deleting doesn't work yet
			<input type="submit" value="Remove Checked" style="color:#e00000" />
			-->
		</form>

	</div> <!-- #content -->

	<? include_once('../templates/includes/footer.html.php') ?>

</div> <!-- #wrap -->

</body>
</html>
