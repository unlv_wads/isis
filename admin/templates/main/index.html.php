<? include_once('templates/includes/head.html.php') ?>

<body>

<div id="wrap">

	<? include_once('templates/includes/header.html.php') ?>

	<? include_once('templates/includes/menu.html.php') ?>

	<div id="content">

		<? if (View::isFlash()): ?>
			<div class="flash">
				<?= View::getFlash() ?>
			</div>
		<? endif ?>

		<h4>Welcome,
			<?= String::websafe(User::getInstance()->getUsername()) ?>!</h4>

		<p>The ISIS Control Panel a graphical front-end to ISIS's main
		configuration file (config.xml.php). It also allows you to add and modify
		maps and collections.</p>

		<p>Choose an option from the menu above.</p>

		<h6 style="text-align:right">ISIS version
			<?= String::websafe(Preference::getInstance()->getVersion()) ?>
			<? if (!$ctrl->isCurrentVersion()): ?>
				<a href="http://digital.library.unlv.edu/isis/">(a newer version is available)</a>
			<? endif ?>
		</h6>

	</div> <!-- #content -->

	<? include_once('templates/includes/footer.html.php') ?>

</div> <!-- #wrap -->

</body>
</html>
