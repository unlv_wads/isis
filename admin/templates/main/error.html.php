<? include_once($_SERVER['DOCUMENT_ROOT'] . '/'
	. trim(Config::ISIS_FOLDER_ROOT, '/')
	. '/admin/templates/includes/head.html.php') ?>

<body>

<div id="wrap">

<? include_once($_SERVER['DOCUMENT_ROOT'] . '/'
	. trim(Config::ISIS_FOLDER_ROOT, '/')
	. '/admin/templates/includes/header.html.php') ?>

	<div id="content">

		<h2 class="title">ISIS Error</h2>

		<? foreach (CustomException::getAll() as $e): ?>
			<p><?= String::websafe($e->getMessage()) ?></p>

			<? if (!$e instanceof PreferencesException): ?>
				<? if (Preference::getInstance()->isDebugMode()): ?>
					<code><pre><?= String::websafe(wordwrap($e->getTraceAsString(), 80)) ?></pre></code>
				<? endif ?>
			<? endif ?>
		<? endforeach ?>

		<? if (!$e instanceof PreferencesException): ?>
			<h5>Version <?= Preference::getInstance()->getVersion() ?>
			<? if (Preference::getInstance()->isDebugMode()): ?>
				&nbsp;&nbsp;<em style="color:red">(DEBUG MODE ON)</em>
			<? endif ?>
			</h5>
		<? endif ?>

		<h6 style="text-align:right">ISIS version
			<?= String::websafe(Preference::getInstance()->getVersion()) ?></h6>

	</div> <!-- #content -->

<? include_once($_SERVER['DOCUMENT_ROOT'] . '/'
	. trim(Config::ISIS_FOLDER_ROOT, '/')
	. '/admin/templates/includes/footer.html.php') ?>

</div> <!-- #wrap -->

</body>
</html>
