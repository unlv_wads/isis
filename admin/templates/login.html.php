<? include_once('includes/head.html.php') ?>

<body>

<div id="wrap">

	<? include_once('templates/includes/header.html.php') ?>

	<div id="content">

		<? if (View::isFlash()): ?>
			<div class="flash">
				<?= View::getFlash() ?>
			</div>
		<? endif ?>

		<form method="post" action="">

			<fieldset>
				<legend>Login</legend>
				<ul class="form">
					<li>
						<label for="username">Username</label>
						<input type="text" id="username" name="username" size="20"
							maxlength="30"/>
					</li>
					<li>
						<label for="password">Password</label>
						<input type="password" id="password" name="password" size="20"
							maxlength="30"/>
					</li>
				</ul>
				<input type="submit" value="Go"/>
			</fieldset>
		</form>

	</div> <!-- #content -->

	<? include_once('includes/footer.html.php') ?>

</div> <!-- wrap -->


</body>
</html>
