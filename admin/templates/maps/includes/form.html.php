<ul class="form" style="float:left" >
	<li>
		<label for="collection">Collection:</label>
		<select name="collection_id" id="collection">
			<? foreach (Collection::getAll() as $c): ?>
				<option style="width:300px"
					value="<?= String::websafe($c->getID()) ?>"
					<? if ($ctrl->getMap()->getCollectionID() == $c->getID()): ?>
						selected="selected"
					<? endif ?>>
					<?= String::websafe($c->getName()) ?>
					(<?= String::websafe($c->getOrganization()) ?>)
				</option>
			<? endforeach ?>
		</select>
	</li>
	<li>
		<label for="ptr">Pointer (&quot;CISOPTR&quot;):</label>
		<input type="text" name="ptr" id="ptr" size="5" maxlength="5"
			value="<?= $ctrl->getMap()->getPtr() ?>" />
	</li>
	<li><label for="bounds">Bounds:</label>
		<table cellspacing="0" id="bounds" cellpadding="0" border="0">
			<tr>
				<td></td>
				<td>
					<label>N Lat:
						<input type="text" name="ln" size="8" maxlength="15"
							value="<?= $ctrl->getMap()->getLatN() ?>" />
					</label>
				</td>
				<td></td>
			</tr>
			<tr>
				<td>
					<label>W Long:
						<input type="text" name="lw" size="8" maxlength="15"
							value="<?= $ctrl->getMap()->getLongW() ?>" />
					</label>
				</td>
				<td></td>
				<td>
					<label>E Long:
						<input type="text" name="le" size="8" maxlength="15"
							value="<?= $ctrl->getMap()->getLongE() ?>" />
					</label>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<label>S Lat:
						<input type="text" name="ls" size="8" maxlength="15"
							value="<?= $ctrl->getMap()->getLatS() ?>" />
					</label>
				</td>
				<td></td>
			</tr>
		</table>
		<a href="http://www.fcc.gov/mb/audio/bickel/DDDMMSS-decimal.html">(D-M-S to decimal degree converter)</a>
	</li>
</ul>
<object style="float:right" width="350" height="250" type="image/svg+xml"
			data="<?= Config::ISIS_FOLDER_ROOT ?>/admin/templates/maps/includes/coordinate_plane.svg" />
