<? include_once('../templates/includes/head.html.php') ?>

<body>

<div id="wrap">

	<? include_once('../templates/includes/header.html.php') ?>

	<? include_once('../templates/includes/menu.html.php') ?>

	<h5 class="breadcrumb">
		<a href="../">Home</a>
		&rArr; <a href="./">Maps</a>
		&rArr; Add
	</h5>

	<div id="content">

		<? if (View::isFlash()): ?>
			<div class="flash">
				<?= View::getFlash() ?>
			</div>
		<? endif ?>

		<form method="post" action="">
			<fieldset>
				<legend>Add a map</legend>

				<? include_once('includes/form.html.php') ?>

			</fieldset>
			<input type="submit" name="add" value="Add" />

		</form>

	</div> <!-- #content -->

	<? include_once('../templates/includes/footer.html.php') ?>

</div> <!-- #wrap -->

</body>
</html>
