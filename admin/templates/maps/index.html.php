<? include_once('../templates/includes/head.html.php') ?>

<body>

<div id="wrap">

	<? include_once('../templates/includes/header.html.php') ?>

	<? include_once('../templates/includes/menu.html.php') ?>

	<h5 class="breadcrumb">
		<a href="../">Home</a>
		&rArr; Maps
	</h5>

	<div id="content">

		<? if (View::isFlash()): ?>
			<div class="flash">
				<?= View::getFlash() ?>
			</div>
		<? endif ?>

		<h5><a href="add.php">Add</a></h5>

		<h4>Page: <?= $ctrl->getView()->getPageLinks($ctrl->getSearch()) ?></h4>

		<form method="post" action="">

			<?= $ctrl->getView()->getFormattedResults($ctrl->getSearch()) ?>
			<!-- deleting doesn't work yet
			<input type="submit" name="delete" value="Remove Checked"
				style="color:#e00000" />
			-->
		</form>

	</div> <!-- #content -->

	<? include_once('../templates/includes/footer.html.php') ?>

</div> <!-- #wrap -->

</body>
</html>
