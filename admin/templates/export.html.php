<? include_once('templates/includes/head.html.php') ?>

<body>

<div id="wrap">

	<? include_once('templates/includes/header.html.php') ?>

	<? include_once('templates/includes/menu.html.php') ?>

	<h5 class="breadcrumb">
		<a href="../">Home</a>
		&rArr; Export
	</h5>

	<div id="content">

		<? if (View::isFlash()): ?>
			<div class="flash">
				<?= View::getFlash() ?>
			</div>
		<? endif ?>

		<p>Items will be exported from the
		<strong><?= String::websafe(DataStore::getInstance()->getName()) ?></strong>
		data store, which is the one that is currently active. To export from a
		different data store, you must enable it instead of this one.</p>

		<form action="" method="get">
			<fieldset>
				<legend>Export Maps</legend>
				<ul class="form">
					<li>
						<label><input type="radio" name="format"
							value="csv"/>CSV text</label>
					</li>
					<li>
						<label><input type="radio" name="format"
							value="tab"/>Tab-delimited text</label>
					</li>
					<li>
						<label><input type="radio" name="format"
							value="svg"/>SVG</label>
					</li>
					<li>
						<label><input type="radio" name="format"
							value="sql"/>ISIS SQL</label>
					</li>
					<li>
						<label><input type="radio" name="format"
							value="xml"/>ISIS XML</label>
					</li>
				</ul>
				<input type="hidden" name="entity" value="maps" />
				<input type="submit" value="Export"/>
			</fieldset>
		</form>

		<form action="" method="get">
			<fieldset>
				<legend>Export Collection Definitions</legend>
				<ul class="form">
					<li>
						<label><input type="radio" name="format"
							value="sql"/>ISIS SQL</label>
					</li>
					<li>
						<label><input type="radio" name="format"
							value="xml"/>ISIS XML</label>
					</li>
				</ul>
				<input type="hidden" name="entity" value="collections" />
				<input type="submit" value="Export"/>
			</fieldset>
		</form>

	</div> <!-- #content -->

	<? include_once('templates/includes/footer.html.php') ?>

</div> <!-- #wrap -->

</body>
</html>
